package com.daeruin.darkestbeforedawn.client;

import com.daeruin.darkestbeforedawn.blocks.BlockRegistry;
import com.daeruin.darkestbeforedawn.items.ItemRegistry;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ModelRegistry
{
    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event)
    {
        PrimalUtilReg.registerBlockModels(BlockRegistry.getBlocks());
        PrimalUtilReg.registerItemModels(ItemRegistry.getItems());
    }
}
