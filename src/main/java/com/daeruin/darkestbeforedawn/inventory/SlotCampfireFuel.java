package com.daeruin.darkestbeforedawn.inventory;

import com.daeruin.darkestbeforedawn.tileentity.TileEntityCampfire;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.ParametersAreNonnullByDefault;

class SlotCampfireFuel extends SlotItemHandler
{

    SlotCampfireFuel(IItemHandler itemHandler, @SuppressWarnings("SameParameterValue") int index, @SuppressWarnings("SameParameterValue") int xPosition, @SuppressWarnings("SameParameterValue") int yPosition)
    {
        super(itemHandler, index, xPosition, yPosition);
    }

    @Override
    @ParametersAreNonnullByDefault
    public boolean isItemValid(ItemStack stack)
    {
        //noinspection SimplifiableIfStatement
        if (TileEntityCampfire.isItemFuel(stack))
        {
            return super.isItemValid(stack);
        }
        else
        {
            return false;
        }
    }

    // Enforces the slot's stack limit for the container's mergeItemStack and transferStackInSlot methods
    @Override
    public int getSlotStackLimit()
    {
        return 4;
    }

}
