package com.daeruin.darkestbeforedawn.network;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketHandler
{
    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel("primal_channel");
    private static int PACKET_INDEX = 0; // Keeps track of packets

    // Every packet has to be registered separately and assigned a unique number on my channel
    // Side is where the message is being sent TO
    public static void registerPackets()
    {
        INSTANCE.registerMessage(CampfirePacket.CampfireHandler.class, CampfirePacket.class, PACKET_INDEX++, Side.CLIENT);
    }
}
