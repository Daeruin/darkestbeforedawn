package com.daeruin.darkestbeforedawn.recipes;

import com.daeruin.darkestbeforedawn.blocks.BlockRegistry;
import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.items.ItemRegistry;
import com.daeruin.primallib.blocks.PrimalBlockRegistry;
import com.google.common.collect.Maps;
import net.minecraft.block.Block;
import net.minecraft.block.BlockNewLog;
import net.minecraft.block.BlockOldLog;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFishFood;
import net.minecraft.item.ItemStack;

import java.util.Map;

public class CampfireRecipes
{
    private static final CampfireRecipes INSTANCE = new CampfireRecipes();
    /**
     * The list of cooking results.
     */
    private final Map<ItemStack, ItemStack> cookingList = Maps.newHashMap();
    /**
     * A list that contains how many experience points each recipe output will give.
     */
    private final Map<ItemStack, Float> experienceList = Maps.newHashMap();
    /**
     * A list that contains how many experience points each recipe output will give.
     */
    private final Map<ItemStack, Integer> cookTimeList = Maps.newHashMap();

    private CampfireRecipes()
    {
        int charcoalAmtLogs = DarkestConfig.itemScarcity.charcoalAmtLogs;
        IBlockState blockStateOak = Blocks.LOG.getDefaultState();
        IBlockState blockStateSpruce = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.SPRUCE);
        IBlockState blockStateBirch = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.BIRCH);
        IBlockState blockStateJungle = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE);
        IBlockState blockStateAcacia = Blocks.LOG2.getDefaultState().withProperty(BlockNewLog.VARIANT, BlockPlanks.EnumType.ACACIA);
        IBlockState blockStateDarkOak = Blocks.LOG2.getDefaultState().withProperty(BlockNewLog.VARIANT, BlockPlanks.EnumType.DARK_OAK);
        int metaOak = Blocks.LOG.getMetaFromState(blockStateOak);
        int metaSpruce = Blocks.LOG.getMetaFromState(blockStateSpruce);
        int metaBirch = Blocks.LOG.getMetaFromState(blockStateBirch);
        int metaJungle = Blocks.LOG.getMetaFromState(blockStateJungle);
        int metaAcacia = Blocks.LOG2.getMetaFromState(blockStateAcacia);
        int metaDarkOak = Blocks.LOG2.getMetaFromState(blockStateDarkOak);

        // Clay bowl, fat and resin
        if (DarkestConfig.basicItems.allowUnlitTorches)
        {
            this.addCampfireRecipe(ItemRegistry.CLAY_BOWL_UNCOOKED, ItemRegistry.CLAY_BOWL, DarkestConfig.campfireBehavior.clayCookTime, 0.5F);
            this.addCampfireRecipe(ItemRegistry.CLAY_BOWL_FAT, ItemRegistry.CLAY_BOWL_FAT_RENDERED, 100, 0.1F);
            this.addCampfireRecipe(ItemRegistry.CLAY_BOWL_RESIN, ItemRegistry.CLAY_BOWL_RESIN_MELTED, 100, 0.1F);
        }

        // Charcoal dust from logs
        this.addCampfireRecipe(new ItemStack(Blocks.LOG, 1, metaOak), new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.oakBurnTime, 0.15F);
        this.addCampfireRecipe(new ItemStack(Blocks.LOG, 1, metaSpruce), new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.spruceBurnTime, 0.15F);
        this.addCampfireRecipe(new ItemStack(Blocks.LOG, 1, metaBirch), new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.birchBurnTime, 0.15F);
        this.addCampfireRecipe(new ItemStack(Blocks.LOG, 1, metaJungle), new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.jungleBurnTime, 0.15F);
        this.addCampfireRecipe(new ItemStack(Blocks.LOG2, 1, metaAcacia), new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.acaciaBurnTime, 0.15F);
        this.addCampfireRecipe(new ItemStack(Blocks.LOG2, 1, metaDarkOak), new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.darkOakBurnTime, 0.15F);
        this.addCampfireRecipe(BlockRegistry.LOG_SPRUCE_RESINOUS, new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.spruceBurnTime, 0.15F);
        this.addCampfireRecipe(PrimalBlockRegistry.STRIPPED_ACACIA_LOG, new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.acaciaBurnTime, 0.15F);
        this.addCampfireRecipe(PrimalBlockRegistry.STRIPPED_BIRCH_LOG, new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.birchBurnTime, 0.15F);
        this.addCampfireRecipe(PrimalBlockRegistry.STRIPPED_DARK_OAK_LOG, new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.darkOakBurnTime, 0.15F);
        this.addCampfireRecipe(PrimalBlockRegistry.STRIPPED_JUNGLE_LOG, new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.jungleBurnTime, 0.15F);
        this.addCampfireRecipe(PrimalBlockRegistry.STRIPPED_OAK_LOG, new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.oakBurnTime, 0.15F);
        this.addCampfireRecipe(PrimalBlockRegistry.STRIPPED_SPRUCE_LOG, new ItemStack(ItemRegistry.DUST_CHARCOAL, charcoalAmtLogs), DarkestConfig.campfireBehavior.spruceBurnTime, 0.15F);
        // Food
        this.addCampfireRecipe(Items.PORKCHOP, new ItemStack(Items.COOKED_PORKCHOP), 200, 0.35F);
        this.addCampfireRecipe(Items.BEEF, new ItemStack(Items.COOKED_BEEF), 200, 0.35F);
        this.addCampfireRecipe(Items.CHICKEN, new ItemStack(Items.COOKED_CHICKEN), 200, 0.35F);
        this.addCampfireRecipe(Items.RABBIT, new ItemStack(Items.COOKED_RABBIT), 200, 0.35F);
        this.addCampfireRecipe(Items.MUTTON, new ItemStack(Items.COOKED_MUTTON), 200, 0.35F);
        this.addCampfireRecipe(Items.POTATO, new ItemStack(Items.BAKED_POTATO), 200, 0.35F);
        for (ItemFishFood.FishType fishType : ItemFishFood.FishType.values())
        {
            if (fishType.canCook())
            {
                this.addCampfireRecipe(new ItemStack(Items.FISH, 1, fishType.getMetadata()), new ItemStack(Items.COOKED_FISH, 1, fishType.getMetadata()), 200, 0.35F);
            }
        }
        // Miscellaneous
        this.addCampfireRecipe(Items.CLAY_BALL, new ItemStack(Items.BRICK), DarkestConfig.campfireBehavior.clayCookTime, 0.3F);
        this.addCampfireRecipe(Blocks.CACTUS, new ItemStack(Items.DYE, 1, EnumDyeColor.GREEN.getDyeDamage()), 200, 0.2F);
        this.addCampfireRecipe(new ItemStack(Blocks.SPONGE, 1, 1), new ItemStack(Blocks.SPONGE, 1, 0), 500, 0.15F);
        this.addCampfireRecipe(Items.CHORUS_FRUIT, new ItemStack(Items.CHORUS_FRUIT_POPPED), 200, 0.1F);
        // this.addCampfireRecipe(Blocks.CLAY, new ItemStack(Blocks.HARDENED_CLAY), 3500, 0.35F);
    }

    /**
     * Returns an instance of CampfireRecipes.
     */
    public static CampfireRecipes instance()
    {
        return INSTANCE;
    }

    /**
     * Adds a campfire recipe, where the input item is an instance of Block.
     */
    private void addCampfireRecipe(Block input, ItemStack result, int cookTime, float experience)
    {
        this.addCampfireRecipe(Item.getItemFromBlock(input), result, cookTime, experience);
    }

    /**
     * Adds a campfire recipe, where the input item is an instance of Item.
     */
    private void addCampfireRecipe(Item input, ItemStack result, int cookTime, float experience)
    {
        this.addCampfireRecipe(new ItemStack(input, 1, 32767), result, cookTime, experience);
    }

    /**
     * Adds a campfire recipe, where the input item is an instance of Item.
     */
    private void addCampfireRecipe(Item input, Item result, int cookTime, float experience)
    {
        this.addCampfireRecipe(new ItemStack(input, 1, 32767), new ItemStack(result), cookTime, experience);
    }

    /**
     * Adds a smelting recipe using an ItemStack as the input for the recipe.
     */
    private void addCampfireRecipe(ItemStack input, ItemStack result, int cookTime, float experience)
    {
        if (getCookingResult(input) != ItemStack.EMPTY)
        {
            net.minecraftforge.fml.common.FMLLog.log.info("Ignored smelting recipe with conflicting input: {} = {}", input, result);
            return;
        }
        this.cookingList.put(input, result);
        this.cookTimeList.put(input, cookTime);
        this.experienceList.put(result, experience);
    }

    /**
     * Returns the smelting result of an item.
     */
    public ItemStack getCookingResult(ItemStack stack)
    {
        for (Map.Entry<ItemStack, ItemStack> entry : this.cookingList.entrySet())
        {
            if (this.compareItemStacks(stack, entry.getKey()))
            {
                return entry.getValue();
            }
        }

        return ItemStack.EMPTY;
    }

    /**
     * Returns the required cooking time for an item.
     */
    public int getTotalCookTime(ItemStack stack)
    {
        for (Map.Entry<ItemStack, Integer> entry : this.cookTimeList.entrySet())
        {
            if (this.compareItemStacks(stack, entry.getKey()))
            {
                return entry.getValue();
            }
        }

        return 0;
    }

    /**
     * Compares two ItemStacks to ensure that they are the same. This checks both the item and the metadata of the item.
     */
    private boolean compareItemStacks(ItemStack stack1, ItemStack stack2)
    {
        // ItemHandlerHelper.canItemStacksStack(stack1, stack2);
        // ItemStack.areItemStacksEqual(stack1, stack2);
        return stack2.getItem() == stack1.getItem() && (stack2.getMetadata() == 32767 || stack2.getMetadata() == stack1.getMetadata());
    }

    // public Map<ItemStack, ItemStack> getCookingList()
    // {
    //     return this.cookingList;
    // }

    public float getCookingExperience(ItemStack stack)
    {
        float ret = stack.getItem().getSmeltingExperience(stack);
        if (ret != -1)
            return ret;

        for (Map.Entry<ItemStack, Float> entry : this.experienceList.entrySet())
        {
            if (this.compareItemStacks(stack, entry.getKey()))
            {
                return entry.getValue();
            }
        }

        return 0.0F;
    }
}
