package com.daeruin.darkestbeforedawn;

import com.daeruin.darkestbeforedawn.blocks.BlockRegistry;
import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.events.EventHandler;
import com.daeruin.darkestbeforedawn.items.ItemRegistry;
import com.daeruin.darkestbeforedawn.network.GuiHandler;
import com.daeruin.darkestbeforedawn.network.PacketHandler;
import com.daeruin.darkestbeforedawn.recipes.OreDictRegistry;
import com.daeruin.darkestbeforedawn.recipes.RecipeHandler;
import com.daeruin.darkestbeforedawn.tileentity.TileEntityCampfire;
import com.daeruin.darkestbeforedawn.tileentity.TileEntityTorch;
import com.daeruin.darkestbeforedawn.utilities.DarkestUtil;
import com.daeruin.darkestbeforedawn.worldgen.TorchGenerator;
import com.daeruin.primallib.util.PrimalCreativeTab;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = DarkestBeforeDawn.MODID,
        name = "Darkest Before Dawn",
        version = "{@version}",
        dependencies = "required-after:primallib",
        acceptedMinecraftVersions = "1.12, 1.12.1, 1.12.2"
)

public class DarkestBeforeDawn
{
    public static final String MODID = "darkestbeforedawn";
    public static final Material PRIMAL_CAMPFIRE = new Material(MapColor.YELLOW)
    {
        public boolean blocksMovement()
        {
            return false; // Flowing water will break this block
        }
    };
    @SuppressWarnings("CanBeFinal")
    @Mod.Instance
    public static DarkestBeforeDawn INSTANCE = new DarkestBeforeDawn();
    @SidedProxy(clientSide = "com.daeruin.darkestbeforedawn.ClientProxy", serverSide = "com.daeruin.darkestbeforedawn.ServerProxy")
    private static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e)
    {
        NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
        PacketHandler.registerPackets();
        GameRegistry.registerTileEntity(TileEntityTorch.class, new ResourceLocation("darkestbeforedawn:darkestbeforedawn_tile_entity_torch"));
        GameRegistry.registerTileEntity(TileEntityCampfire.class, new ResourceLocation("darkestbeforedawn:darkestbeforedawn_tile_entity_campfire"));
        // GameRegistry.registerTileEntity(TileEntityDarkestFurnace.class, new ResourceLocation("darkestbeforedawn:darkestbeforedawn_tile_entity_furnace"));
        GameRegistry.registerWorldGenerator(new TorchGenerator(), 0);

        proxy.preInit();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e)
    {
        OreDictRegistry.registerOreDictEntries();
        RecipeHandler.removeConfigRecipes();

        // RecipeHandler.registerSmeltingRecipes();
        if (DarkestConfig.campfireBehavior.removeFoodFromFurnace)
        {
            RecipeHandler.removeSmeltingRecipes();
        }

        PrimalCreativeTab.addBlocksToSortOrder(BlockRegistry.getBlocks());
        PrimalCreativeTab.addItemsToSortOrder(ItemRegistry.getItems());
        proxy.init();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e)
    {
        MinecraftForge.EVENT_BUS.register(new EventHandler());
        DarkestUtil.validateTorchReplacement();

        proxy.postInit();
    }
}
