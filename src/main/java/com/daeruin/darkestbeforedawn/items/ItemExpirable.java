package com.daeruin.darkestbeforedawn.items;

import com.daeruin.primallib.items.ItemBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

class ItemExpirable extends ItemBase
{
    private ItemStack replacementItem;

    ItemExpirable(String registryName, int maxDamage, ItemStack replacementItem)
    {
        super(registryName);
        this.setMaxDamage(maxDamage); // Max damage is the number of ticks before the item is destroyed
        this.replacementItem = replacementItem;
    }

    @Override
    @SuppressWarnings("deprecation")
    public int getEntityLifespan(ItemStack itemStack, World world)
    {
        return itemStack.getMaxDamage() - itemStack.getItemDamage();
    }

    @Override
    public void onUpdate(ItemStack itemStack, World world, Entity entity, int itemSlot, boolean isSelected)
    {
        if (!world.isRemote && this.getMaxDamage(itemStack) > 0)
        {
            if (entity instanceof EntityPlayerMP && !((EntityPlayer) entity).isCreative())
            {
                // ItemStack#damageItem also works but makes a tool-breaking sound and doesn't replace item
                if (itemStack.attemptDamageItem(1, world.rand, (EntityPlayerMP) entity))
                {
                    entity.replaceItemInInventory(itemSlot, replacementItem);
                }
            }
        }
    }
}
