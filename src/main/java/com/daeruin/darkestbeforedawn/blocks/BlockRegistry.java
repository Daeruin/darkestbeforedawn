package com.daeruin.darkestbeforedawn.blocks;

import com.daeruin.primallib.blocks.PrimalBlockLog;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.block.Block;
import net.minecraft.block.BlockOldLog;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import javax.annotation.Nonnull;
import java.util.LinkedHashSet;

public final class BlockRegistry
{
    // public static final Block FURNACE = new BlockDarkestFurnace("furnace", false);
    // public static final Block FURNACE_LIT = new BlockDarkestFurnace("lit_furnace", true).setCreativeTab(null);
    public static final Block LOG_SPRUCE_RESINOUS = new PrimalBlockLog("log_spruce_resinous")
    {
        @Override
        public void getDrops(@Nonnull NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, @Nonnull IBlockState state, int fortune)
        {
            Item item = Item.getItemFromBlock(Blocks.LOG);
            int meta = Blocks.LOG.getMetaFromState(Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.SPRUCE));
            drops.add(new ItemStack(item, 1, meta));
        }
    };
    public static final Block TORCH_UNLIT = new BlockTorchUnlit("torch_unlit");
    public static final Block TORCH_LIT = new BlockTileEntityTorchLit("torch_lit");
    public static final Block CAMPFIRE = new BlockTileEntityCampfire("campfire");
    private static final Block FIREBOARD = new BlockFireboard("fireboard");

    public static LinkedHashSet<Block> getBlocks()
    {
        LinkedHashSet<Block> BLOCKS = new LinkedHashSet<>();

        BLOCKS.add(FIREBOARD);
        BLOCKS.add(TORCH_UNLIT);
        BLOCKS.add(TORCH_LIT);
        BLOCKS.add(CAMPFIRE);
        // BLOCKS.add(FURNACE);
        // BLOCKS.add(FURNACE_LIT);
        BLOCKS.add(LOG_SPRUCE_RESINOUS);

        return BLOCKS;
    }

    @Mod.EventBusSubscriber
    public static class RegistrationHandler
    {
        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event)
        {
            PrimalUtilReg.registerBlocks(event, getBlocks());
        }

        @SubscribeEvent
        public static void registerItemBlocks(RegistryEvent.Register<Item> event)
        {
            PrimalUtilReg.registerItemBlocks(event, getBlocks());
        }
    }
}
