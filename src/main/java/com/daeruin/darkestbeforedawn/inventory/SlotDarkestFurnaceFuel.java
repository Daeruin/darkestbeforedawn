package com.daeruin.darkestbeforedawn.inventory;

import com.daeruin.darkestbeforedawn.tileentity.TileEntityDarkestFurnace;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.ItemStack;

public class SlotDarkestFurnaceFuel extends SlotFurnaceFuel
{
    SlotDarkestFurnaceFuel(IInventory inventoryIn, int slotIndex, int xPosition, int yPosition)
    {
        super(inventoryIn, slotIndex, xPosition, yPosition);
    }

    @Override
    public boolean isItemValid(ItemStack stack)
    {
        // System.out.println("Fuel slot checking if item is valid");
        return TileEntityDarkestFurnace.getFuelBurnTime(stack) > 0 || isBucket(stack);
    }
}
