package com.daeruin.darkestbeforedawn.worldgen;

import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import net.minecraft.block.Block;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class TorchGenerator implements IWorldGenerator
{
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider)
    {
        if (!DarkestConfig.basicItems.allowVanillaTorches)
        {
            int cornerXInChunk = (chunkX * 16); // Convert chunk coordinates to absolute coordinates (world coordinates)
            int cornerZInChunk = (chunkZ * 16); // Convert chunk coordinates to absolute coordinates (world coordinates)

            for (int x = 0; x < 16; x++)
            {
                for (int y = 0; y < world.getHeight(); y++)
                {
                    for (int z = 0; z < 16; z++)
                    {
                        BlockPos posToCheck = new BlockPos(cornerXInChunk + x, y, cornerZInChunk + z);
                        IBlockState stateToCheck = world.getBlockState(posToCheck);
                        Block blockToCheck = stateToCheck.getBlock();

                        // If we find a vanilla torch during world gen, replace it with the block supplied to the config
                        // If no block is supplied to the config, or if block supplied is unrecognizable, delete the vanilla torch
                        if (blockToCheck instanceof BlockTorch)
                        {
                            // System.out.println("Found vanilla torch");
                            Block blockFromConfig = Block.getBlockFromName(DarkestConfig.basicItems.replacementForVanillaTorches);
                            // System.out.println("Block from config " + blockFromConfig);

                            if (blockFromConfig != null)
                            {
                                // System.out.println("Replacing vanilla block with " + blockFromConfig.getRegistryName() + " at " + posToCheck);
                                int torchPosition = blockToCheck.getMetaFromState(stateToCheck);
                                @SuppressWarnings("deprecation")
                                IBlockState targetState = blockFromConfig.getStateFromMeta(torchPosition);

                                world.setBlockState(posToCheck, targetState);
                            }
                            else
                            {
                                // System.out.println("An invalid block, or no block at all, was supplied to the config - deleting vanilla torch at " + posToCheck);
                                world.setBlockToAir(posToCheck);
                            }
                        }
                    }
                }
            }
        }
    }
}
