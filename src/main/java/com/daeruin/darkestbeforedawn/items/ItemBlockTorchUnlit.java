package com.daeruin.darkestbeforedawn.items;

import com.daeruin.darkestbeforedawn.blocks.BlockRegistry;
import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.utilities.DarkestUtil;
import com.daeruin.primallib.util.PrimalUtil;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static com.daeruin.darkestbeforedawn.blocks.BlockTileEntityCampfire.CAMPFIRE_STATE;
import static com.daeruin.darkestbeforedawn.blocks.BlockTileEntityCampfire.EnumCampfireState;

public class ItemBlockTorchUnlit extends ItemBlock
{
    public ItemBlockTorchUnlit()
    {
        super(BlockRegistry.TORCH_UNLIT);
    }

    // Eventually I need to allow torches to be placed in a torch bracket
    @Override
    @Nonnull
    @ParametersAreNonnullByDefault
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        // Place torches

        if (DarkestUtil.isValidTorchPlacement(world, pos))
        {
            return super.onItemUse(player, world, pos, hand, facing, hitX, hitY, hitZ);
        }

        // Light torches

        else if (!world.isRemote)
        {
            for (String blockName : DarkestConfig.torchBehavior.ignitionSourcesForItem)
            {
                Block ignitionBlock = Block.getBlockFromName(blockName);
                boolean shouldIgnite = false;

                if (ignitionBlock != null)
                {
                    IBlockState state = world.getBlockState(pos);
                    Block blockClickedOn = state.getBlock();

                    if (ignitionBlock == Blocks.FIRE)
                    {
                        Block adjacentBlock = world.getBlockState(pos.offset(facing)).getBlock();

                        if (adjacentBlock == ignitionBlock)
                        {
                            shouldIgnite = true;
                        }
                    }
                    // For this to work, you have to SNEAK-right-click on the campfire
                    else if (ignitionBlock == BlockRegistry.CAMPFIRE && blockClickedOn == BlockRegistry.CAMPFIRE)
                    {
                        EnumCampfireState campfireState = state.getValue(CAMPFIRE_STATE);

                        if (campfireState == EnumCampfireState.BURNING || campfireState == EnumCampfireState.COALS)
                        {
                            shouldIgnite = true;
                        }
                    }
                    else if (ignitionBlock == Blocks.LAVA || ignitionBlock == Blocks.FLOWING_LAVA)
                    {
                        RayTraceResult ray = PrimalUtil.getRayTraceResultFromPlayer(world, player);

                        if (ray != null && ray.typeOfHit == RayTraceResult.Type.BLOCK)
                        {
                            BlockPos rayBlockPos = ray.getBlockPos();
                            Material rayMaterial = world.getBlockState(rayBlockPos).getMaterial();

                            if (rayMaterial == Material.LAVA)
                            {
                                shouldIgnite = true;
                            }
                        }
                    }
                    else if (blockClickedOn == ignitionBlock)
                    {
                        shouldIgnite = true;
                    }

                    if (shouldIgnite)
                    {
                        player.getHeldItem(hand).shrink(1);
                        player.addItemStackToInventory(new ItemStack(BlockRegistry.TORCH_LIT));
                        world.playSound(null, pos, SoundEvents.BLOCK_FIRE_AMBIENT, SoundCategory.BLOCKS, 1.0F, world.rand.nextFloat() * 0.1F + 0.9F);

                        return EnumActionResult.SUCCESS;
                    }
                }
                else
                {
                    System.out.println("Invalid ignition source in Darkest Before Dawn config.");
                }
            }
        }

        return EnumActionResult.FAIL;
    }
}
