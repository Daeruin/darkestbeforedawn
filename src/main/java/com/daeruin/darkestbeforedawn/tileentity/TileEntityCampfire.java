package com.daeruin.darkestbeforedawn.tileentity;

import com.daeruin.darkestbeforedawn.blocks.BlockRegistry;
import com.daeruin.darkestbeforedawn.blocks.BlockTileEntityCampfire;
import com.daeruin.darkestbeforedawn.blocks.BlockTileEntityCampfire.EnumCampfireState;
import com.daeruin.darkestbeforedawn.blocks.BlockTorchUnlit;
import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.inventory.ItemStackHandlerCampfire;
import com.daeruin.darkestbeforedawn.recipes.CampfireRecipes;
import com.daeruin.primallib.blocks.PrimalBlockRegistry;
import com.daeruin.primallib.util.PrimalUtil;
import net.minecraft.block.Block;
import net.minecraft.block.BlockNewLog;
import net.minecraft.block.BlockOldLog;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

public class TileEntityCampfire extends TileEntity implements ITickable
{
    // Cook time constants
    // private static final int MAX_COOK_TIME = 200;
    private static final int MAX_COAL_TIME = 3600;
    // Slot IDs
    public static final int SLOT_INPUT = 0;
    public static final int SLOT_FUEL = 1;
    @SuppressWarnings("WeakerAccess")
    public static final int SLOT_OUTPUT = 2;
    // Field IDs
    public static final int FIELD_BURN_TIME_INITIAL = 0;
    public static final int FIELD_BURN_TIME_REMAINING = 1;
    public static final int FIELD_COOK_TIME = 2;
    public static final int FIELD_TOTAL_COOK_TIME = 3;
    private final ItemStackHandler inventory;
    // Field values
    private int burnTimeInitial; // The number of ticks a fresh copy of the currently-burning item would keep the furnace burning for
    private int burnTimeRemaining; // The number of ticks the furnace will keep burning
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private int cookTimer; // The number of ticks the current item has been cooking for
    private int totalCookTime; // The total number of ticks it will take to cook the current item
    // Campfire state trackers
    private int stokeTimer; // The number of ticks since player last blew on the campfire - when it reaches 0, stoked tinder/kindling reverts to regular tinder/kindling
    private int timesBlown; // The number of times player has activated (right clicked) stoked tinder and kindling
    private int coalsCoolDown; // The number of ticks before coals revert to ashes - based on how many logs have been burned

    public TileEntityCampfire()
    {
        this.inventory = new ItemStackHandlerCampfire(this);
    }

    @SideOnly(Side.CLIENT)
    public static boolean isBurning(TileEntityCampfire inventory)
    {
        return inventory.getField(FIELD_BURN_TIME_REMAINING) > 0;
    }

    // Returns true if the item has a burn time, or false otherwise
    public static boolean isItemFuel(ItemStack stack)
    {
        // System.out.println("Item burn time: " + getItemBurnTime(stack));
        return getItemBurnTime(stack) > 0;
    }

    // Looks up the specified fuel item and returns burn time
    private static int getItemBurnTime(ItemStack stack)
    {
        if (!stack.isEmpty())
        {
            Item itemToBurn = stack.getItem();

            for (String campfireFuel : DarkestConfig.campfireBehavior.campfireFuel)
            {
                try
                {
                    String[] campfireFuelParsed = campfireFuel.split(",");

                    if (campfireFuelParsed.length == 0)
                    {
                        System.err.println("Campfire fuel " + campfireFuel + " is not structured correctly. Please fix your config file.");
                        continue;
                    }

                    Item itemFromConfig;
                    String fuelName = campfireFuelParsed[0];
                    int fuelBurnTime = Integer.parseInt(campfireFuelParsed[1]);
                    Block blockFromConfig = Block.getBlockFromName(fuelName);

                    if (blockFromConfig != null)
                    {
                        itemFromConfig = Item.getItemFromBlock(blockFromConfig);
                    }
                    else
                    {
                        itemFromConfig = Item.getByNameOrId(fuelName);
                    }

                    if (itemFromConfig != null)
                    {
                        if (itemToBurn == itemFromConfig)
                        {
                            return fuelBurnTime;
                        }
                    }
                }
                catch (Exception exception)
                {
                    System.err.println("Failed to parse " + campfireFuel + ". " + exception);
                }
            }

            Block block = Block.getBlockFromItem(itemToBurn);

            if (block instanceof BlockOldLog || block instanceof BlockNewLog)
            {
                int meta = stack.getMetadata();
                @SuppressWarnings("deprecation") IBlockState state = block.getStateFromMeta(meta);
                BlockPlanks.EnumType logType = PrimalUtil.getLogEnumType(state);

                switch (logType)
                {
                    case ACACIA:
                        return DarkestConfig.campfireBehavior.acaciaBurnTime;
                    case BIRCH:
                        return DarkestConfig.campfireBehavior.birchBurnTime;
                    case DARK_OAK:
                        return DarkestConfig.campfireBehavior.darkOakBurnTime;
                    case JUNGLE:
                        return DarkestConfig.campfireBehavior.jungleBurnTime;
                    case OAK:
                        return DarkestConfig.campfireBehavior.oakBurnTime;
                    case SPRUCE:
                        return DarkestConfig.campfireBehavior.darkOakBurnTime;
                }
            }
            else if (block == PrimalBlockRegistry.STRIPPED_ACACIA_LOG)
                return DarkestConfig.campfireBehavior.acaciaBurnTime;
            else if (block == PrimalBlockRegistry.STRIPPED_BIRCH_LOG)
                return DarkestConfig.campfireBehavior.birchBurnTime;
            else if (block == PrimalBlockRegistry.STRIPPED_DARK_OAK_LOG)
                return DarkestConfig.campfireBehavior.darkOakBurnTime;
            else if (block == PrimalBlockRegistry.STRIPPED_JUNGLE_LOG)
                return DarkestConfig.campfireBehavior.jungleBurnTime;
            else if (block == PrimalBlockRegistry.STRIPPED_OAK_LOG)
                return DarkestConfig.campfireBehavior.oakBurnTime;
            else if (block == PrimalBlockRegistry.STRIPPED_SPRUCE_LOG)
                return DarkestConfig.campfireBehavior.spruceBurnTime;
            else if (block == BlockRegistry.LOG_SPRUCE_RESINOUS)
                return DarkestConfig.campfireBehavior.spruceBurnTime;
        }

        return 0;
    }

    public int getField(int id)
    {
        switch (id)
        {
            case FIELD_BURN_TIME_INITIAL:
                return this.getBurnTimeInitial();
            case FIELD_BURN_TIME_REMAINING:
                return this.getBurnTimeRemaining();
            case FIELD_COOK_TIME:
                return this.getCookTimer();
            case FIELD_TOTAL_COOK_TIME:
                return this.getTotalCookTime();
            default:
                return 0;
        }
    }

    public void setField(int id, int value)
    {
        switch (id)
        {
            case FIELD_BURN_TIME_INITIAL:
                this.setBurnTimeInitial(value);
                break;
            case FIELD_BURN_TIME_REMAINING:
                this.setBurnTimeRemaining(value);
                break;
            case FIELD_COOK_TIME:
                this.setCookTimer(value);
                break;
            case FIELD_TOTAL_COOK_TIME:
                this.setTotalCookTime(value);
        }
    }

    @SuppressWarnings("SameReturnValue")
    public int getFieldCount()
    {
        return 4;
    }

    private boolean isBurning()
    {
        return this.getBurnTimeRemaining() > 0;
    }

    public void update()
    {
        boolean isBurningAtStart = this.isBurning();
        boolean stateHasChanged = false;
        EnumCampfireState campfireState = world.getBlockState(this.pos).getValue(BlockTileEntityCampfire.CAMPFIRE_STATE);

        if (this.isBurning())
        {
            this.setBurnTimeRemaining(this.getBurnTimeRemaining() - 1); // If currently burning, reduce burn time by 1
            // System.out.println("Reduce burn time: " + getBurnTimeRemaining());

            // If raining, fuel will run out 7 times faster - eventually I want to store unused burnTime for use next time campfire is lit
            if (this.world.isRainingAt(this.getPos().up()))
            {
                this.setBurnTimeRemaining(MathHelper.clamp(this.getBurnTimeRemaining() - 7, 0, this.getBurnTimeInitial()));
            }
        }

        if (!this.world.isRemote) // If on server
        {
            ItemStack fuelItem = this.inventory.getStackInSlot(SLOT_FUEL);
            ItemStack outputItem = this.inventory.getStackInSlot(SLOT_OUTPUT);

            // If fuel runs out, turn into coals
            if (campfireState == EnumCampfireState.BURNING && !this.isBurning())
            {
                // System.out.println("Burning state but no burnTimeRemaining");
                if (this.world.isRainingAt(this.getPos().up()) && !fuelItem.isEmpty())
                {
                    // System.out.println("Raining and has fuel - delete and spawn fuel");
                    // Delete fuel from fuel slot and spawn into world
                    this.inventory.extractItem(SLOT_FUEL, fuelItem.getCount(), false);
                    Entity entity = new EntityItem(world, this.getPos().getX(), this.getPos().getY(), this.getPos().getZ(), fuelItem);
                    world.spawnEntity(entity);
                }
                // If fuel has run out (or been expelled by rain), turn fire into coals
                if (fuelItem.isEmpty())
                {
                    BlockTileEntityCampfire.setCampfireState(world, this.pos, EnumCampfireState.COALS);
                }
            }

            // Reduce stoke time
            if (campfireState == EnumCampfireState.TINDER_STOKED || campfireState == EnumCampfireState.KINDLING_STOKED)
            {
                if (this.getStokeTimer() > 0)
                {
                    this.setStokeTimer(this.getStokeTimer() - 1);
                }
                // Reduce stoke time and revert to non-stoked state if time runs out
                if (this.getStokeTimer() == 0)
                {
                    this.setTimesBlown(0);
                    if (campfireState == EnumCampfireState.TINDER_STOKED)
                        BlockTileEntityCampfire.setCampfireState(world, this.pos, EnumCampfireState.TINDER);
                    if (campfireState == EnumCampfireState.KINDLING_STOKED)
                        BlockTileEntityCampfire.setCampfireState(world, this.pos, EnumCampfireState.KINDLING);
                    // PrimalHints.sendHint(world.getClosestPlayer(pos.getX(), pos.getY(), pos.getZ(), 5, false), PrimalHints.KEEP_BLOWING);
                }
            }

            // IF CURRENTLY BURNING, OR FUEL IS FULL

            // Note that fuel slot only accepts approved fuel items, so if it has something in it, it can burn by definition
            // Vanilla furnace requires something in input slot in order to burn, but I removed that - not realistic
            if (this.isBurning() || !fuelItem.isEmpty())
            {
                // If not burning yet, and not raining, set burn timer and reduce fuel stack
                // Can't get here unless there's fuel in the fuel slot, so this statement only activates if fuel was just barely put in
                if (!this.isBurning() && !this.world.isRainingAt(this.getPos().up()))
                {
                    this.setBurnTimeRemaining(getItemBurnTime(fuelItem)); // Start burning
                    this.setBurnTimeInitial(this.getBurnTimeRemaining());
                    // System.out.println("Starting to burn with " + this.getBurnTimeRemaining() + " ticks remaining");

                    if (this.isBurning()) // Should now be burning, because burnTimeRemaining has a value (used to check canSmelt - removed)
                    {
                        stateHasChanged = true;
                        if (PrimalUtil.isLog(Block.getBlockFromItem(fuelItem.getItem())))
                        {
                            this.setCoalsCoolDown(MathHelper.clamp(this.getCoalsCoolDown() + 600, 0, MAX_COAL_TIME));
                        }
                        this.inventory.extractItem(SLOT_FUEL, 1, false);
                    }
                }

                // If already burning, and we can smelt: increase cook time and check to produce output
                if (this.isBurning() && this.canSmelt())
                {
                    // System.out.println("Currently burning and input item can be cooked");
                    ItemStack inputStack = this.inventory.getStackInSlot(SLOT_INPUT);

                    // this.setTotalCookTimeForInput(inputStack);
                    this.setCookTimer(this.getCookTimer() + 1);
                    // System.out.println("Increase cook timer: " + getCookTimer() + "; total cook time: " + getTotalCookTime());

                    if (/*this.getCookTimer() != 0 &&*/ this.getCookTimer() == this.getTotalCookTime())
                    {
                        this.setCookTimer(0);
                        // this.setTotalCookTime(0);
                        this.setTotalCookTimeForInput(inputStack);
                        this.smeltItem();
                        stateHasChanged = true;
                        // System.out.println("Item finished cooking! Cook timer: " + getCookTimer() + "; total cook time: " + getTotalCookTime());
                    }
                }
                else
                {
                    // System.out.println("Not burning, or input item can't be cooked. Setting cook timer to 0.");
                    this.setCookTimer(0);
                }
            }
            // If stopped burning partway through being cooked
            else if (!this.isBurning() && this.getCookTimer() > 0)
            {
                if (!(campfireState == EnumCampfireState.COALS && Math.random() < .75)) // If coals, items will un-cook 75% slower
                    this.setCookTimer(MathHelper.clamp(this.getCookTimer() - 1, 0, this.getTotalCookTime()));
            }

            if (campfireState == EnumCampfireState.COALS)
            {
                if (this.getCoalsCoolDown() > 0)
                {
                    this.setCoalsCoolDown(this.getCoalsCoolDown() - 1);

                    // If raining, coals will run out 7 times faster
                    if (this.world.isRainingAt(this.getPos().up()))
                    {
                        this.setCoalsCoolDown(MathHelper.clamp(this.getCoalsCoolDown() - 5, 0, MAX_COAL_TIME));
                    }
                }

                // COALS --> ASHES

                if (this.getCoalsCoolDown() <= 0)
                {
                    this.setBurnTimeInitial(0);
                    this.setCoalsCoolDown(0);
                    BlockTileEntityCampfire.setCampfireState(world, pos, EnumCampfireState.ASHES);

                    // Expel items in slots (although the fuel slot should already be empty, by definition)
                    if (!outputItem.isEmpty())
                    {
                        expelInventory();
                    }
                }
            }

            if (isBurningAtStart != this.isBurning())
            {
                stateHasChanged = true;
                this.setStokeTimer(0);
                this.setTimesBlown(0);
                BlockTileEntityCampfire.setCampfireState(world, this.pos, EnumCampfireState.BURNING);
            }
        }

        if (stateHasChanged)
        {
            this.markDirty();
        }
    }


    public void expelInventory()
    {
        for (int i = 0; i < this.inventory.getSlots(); i++)
        {
            ItemStack stackInSlot = this.inventory.getStackInSlot(i);

            if (!stackInSlot.isEmpty() && stackInSlot.getCount() != 0)
            {
                this.inventory.extractItem(i, stackInSlot.getCount(), false);
                Entity entity = new EntityItem(world, this.getPos().getX(), this.getPos().getY(), this.getPos().getZ(), stackInSlot);
                world.spawnEntity(entity);
            }
        }
    }

    // Returns true if the furnace can smelt an item, in other words:
    // * input slot has something in it
    // * input slot has something with a smelting result
    // * output slot is empty
    // * or output slot already contains the same as the input's smelting result & stack limits aren't exceeded
    private boolean canSmelt()
    {
        ItemStack inputStack = this.inventory.getStackInSlot(SLOT_INPUT);

        if (inputStack.isEmpty())
        {
            return false;
        }
        else
        {
            ItemStack smeltingResultOfInput = CampfireRecipes.instance().getCookingResult(inputStack);
            ItemStack outputStack = this.inventory.getStackInSlot(SLOT_OUTPUT);

            if (smeltingResultOfInput.isEmpty())
                return false; // If input has no smelting result
            if (outputStack.isEmpty())
                return true; // If output slot is empty
            if (!outputStack.isItemEqual(smeltingResultOfInput))
                return false; // If item in output is different than the prospective smelting result

            int combinedSize = outputStack.getCount() + smeltingResultOfInput.getCount();

            // If stack limits aren't exceeded, return true
            return combinedSize <= ((ItemStackHandlerCampfire) inventory).getSlotStackLimit(SLOT_OUTPUT) // Respect stack limit of output slot
                    && combinedSize <= outputStack.getMaxStackSize(); // Respect stack limit of resulting output item
        }
    }

    // Turn one item from the input stack into the appropriate smelted item in the output stack
    private void smeltItem()
    {
        ItemStack outputStack = this.inventory.getStackInSlot(SLOT_OUTPUT);
        ItemStack inputStack = this.inventory.getStackInSlot(SLOT_INPUT);

        if (this.canSmelt())
        {
            ItemStack smeltingResult = CampfireRecipes.instance().getCookingResult(inputStack);

            // I tried replacing this with insertItem, but it didn't work. Not sure why.
            if (outputStack.isEmpty())
            {
                this.inventory.setStackInSlot(SLOT_OUTPUT, smeltingResult.copy());
            }
            else if (/*ItemHandlerHelper.canItemStacksStack(outputStack, inputStack))//*/ outputStack.getItem() == smeltingResult.getItem())
            {
                outputStack.grow(smeltingResult.getCount());
            }

            this.inventory.extractItem(SLOT_INPUT, 1, false);
        }
    }

    // Attempts to insert the player's held item into the appropriate slot in the campfire
    // Returns false if the item is not valid fuel or input, or if the slot was already full
    public boolean insertHeldItem(EntityPlayer player, EnumHand hand)
    {
        ItemStack heldItem = player.getHeldItem(hand);
        int destinationSlot;

        if (isItemFuel(heldItem) && !(Block.getBlockFromItem(heldItem.getItem()) instanceof BlockTorchUnlit))
            destinationSlot = SLOT_FUEL;
        else if (!CampfireRecipes.instance().getCookingResult(heldItem).isEmpty() & this.isBurning())
            destinationSlot = SLOT_INPUT;
        else
            return false;

        ItemStack remainder = inventory.insertItem(destinationSlot, heldItem, true);
        int initialCount = heldItem.getCount();

        if (remainder.getCount() < initialCount)
        {
            inventory.insertItem(destinationSlot, heldItem, false);
            player.setHeldItem(hand, remainder);
            this.markDirty();
            return true;
        }

        return false;
    }

    //	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn)
    //	{
    //		return new PrimalContainerCampfire(playerInventory, this);
    //	}

    @Override
    @ParametersAreNonnullByDefault
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
    {
        // If the old and new block are the same, return false to prevent tile entity from refreshing
        return !(oldState.getBlock() instanceof BlockTileEntityCampfire && newState.getBlock() instanceof BlockTileEntityCampfire);
    }

    @Override
    public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? (T) inventory : super.getCapability(capability, facing);
    }

    @Override
    @Nonnull
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        compound.setInteger("BurnTime", this.getBurnTimeRemaining());
        compound.setInteger("BurnTimeInit", this.getBurnTimeInitial());
        compound.setInteger("CookTime", this.getCookTimer());
        compound.setInteger("CookTimeTotal", this.getTotalCookTime());
        compound.setInteger("TinderStokeTime", this.getStokeTimer());
        compound.setInteger("TinderTimesBlown", this.getTimesBlown());
        compound.setTag("inventory", inventory.serializeNBT());
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        this.setBurnTimeRemaining(compound.getInteger("BurnTime"));
        this.setBurnTimeInitial(compound.getInteger("BurnTimeInit"));
        this.setCookTimer(compound.getInteger("CookTime"));
        this.setTotalCookTime(compound.getInteger("CookTimeTotal"));
        this.setStokeTimer(compound.getInteger("TinderStokeTime"));
        this.setTimesBlown(compound.getInteger("TinderTimesBlown"));
        inventory.deserializeNBT(compound.getCompoundTag("inventory"));
        super.readFromNBT(compound);
    }

    @Override
    @Nullable
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(this.pos, 3, this.getUpdateTag());
    }

    @Override
    @Nonnull
    public NBTTagCompound getUpdateTag()
    {
        return this.writeToNBT(new NBTTagCompound());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
        super.onDataPacket(net, pkt);
        handleUpdateTag(pkt.getNbtCompound());
    }

    // Making a campfire

    public int getStokeTimer()
    {
        return stokeTimer;
    }

    public void setStokeTimer(int stokeTimer)
    {
        this.stokeTimer = stokeTimer;
    }

    public int getTimesBlown()
    {
        return timesBlown;
    }

    public void setTimesBlown(int timesBlown)
    {
        this.timesBlown = timesBlown;
    }

    // Burning fuel

    public int getBurnTimeInitial()
    {
        return burnTimeInitial;
    }

    public void setBurnTimeInitial(int burnTimeInitial)
    {
        this.burnTimeInitial = burnTimeInitial;
    }

    public int getBurnTimeRemaining()
    {
        return burnTimeRemaining;
    }

    public void setBurnTimeRemaining(int burnTimeRemaining)
    {
        this.burnTimeRemaining = burnTimeRemaining;
    }

    private int getCoalsCoolDown()
    {
        return coalsCoolDown;
    }

    public void setCoalsCoolDown(int coalsCoolDown)
    {
        this.coalsCoolDown = coalsCoolDown;
    }

    // Cooking things

    public int getCookTimer()
    {
        return cookTimer;
    }

    public void setCookTimer(int cookTimer)
    {
        this.cookTimer = cookTimer;
    }

    public int getTotalCookTime()
    {
        return totalCookTime;
    }

    private void setTotalCookTime(int totalCookTime)
    {
        this.totalCookTime = totalCookTime;
        // System.out.println("Setting total cook time to: " + this.totalCookTime);
    }

    public void setTotalCookTimeForInput(ItemStack stack)
    {
        this.totalCookTime = CampfireRecipes.instance().getTotalCookTime(stack);
        // System.out.println("Setting total cook time to: " + this.totalCookTime);
    }
}
