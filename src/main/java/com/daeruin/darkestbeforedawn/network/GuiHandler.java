package com.daeruin.darkestbeforedawn.network;

import com.daeruin.darkestbeforedawn.client.GuiCampfire;
import com.daeruin.darkestbeforedawn.inventory.ContainerCampfire;
import com.daeruin.darkestbeforedawn.tileentity.TileEntityCampfire;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler
{
    private static int modGuiIndex = 0; // Keeps track of GUIs
    public static final int GUI_CAMPFIRE = modGuiIndex++; // Sets custom inventory GUI index to next available GUI index
    // public static final int GUI_FURNACE = modGuiIndex++;

    @Override
    public Object getServerGuiElement(int guiID, EntityPlayer player, World world, int x, int y, int z)
    {
        if (guiID == GUI_CAMPFIRE)
        {
            return new ContainerCampfire(player.inventory, (TileEntityCampfire) world.getTileEntity(new BlockPos(x, y, z)));
        }
        // else if (guiID == GUI_FURNACE)
        // {
        //     return new ContainerDarkestFurnace(player.inventory, (TileEntityDarkestFurnace) world.getTileEntity(new BlockPos(x, y, z)));
        // }

        return null;
    }

    @Override
    public Object getClientGuiElement(int guiID, EntityPlayer player, World world, int x, int y, int z)
    {
        if (guiID == GUI_CAMPFIRE)
        {
            return new GuiCampfire(player.inventory, (TileEntityCampfire) world.getTileEntity(new BlockPos(x, y, z)));
        }
        // else if (guiID == GUI_FURNACE)
        // {
        //     return new GuiDarkestFurnace(player.inventory, (TileEntityDarkestFurnace) world.getTileEntity(new BlockPos(x, y, z)));
        // }

        return null;
    }
}
