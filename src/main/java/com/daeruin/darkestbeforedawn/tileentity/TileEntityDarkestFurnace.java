package com.daeruin.darkestbeforedawn.tileentity;

import com.daeruin.darkestbeforedawn.blocks.BlockDarkestFurnace;
import com.daeruin.darkestbeforedawn.inventory.ContainerDarkestFurnace;
import com.daeruin.darkestbeforedawn.items.ItemRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.*;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

public class TileEntityDarkestFurnace extends TileEntityFurnace
{
    private static final int SLOT_INPUT = 0;
    private static final int SLOT_FUEL = 1;
    private static final int SLOT_OUTPUT = 2;
    // Which slots are available from certain faces - probably used for hoppers/chests.
    private NonNullList<ItemStack> furnaceItemStacks = NonNullList.withSize(3, ItemStack.EMPTY); // The items currently being used in the furnace
    private int furnaceBurnTime; //The number of ticks that the furnace will keep burning
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private int currentItemBurnTime; // The number of ticks that a fresh copy of the currently-burning item would keep the furnace burning for.
    private int cookTime;
    private int totalCookTime;
    private int stokeTime;

    /**
     * Returns the number of ticks that the supplied fuel item will keep the furnace burning, or 0 if the item isn't
     * fuel
     */
    public static int getFuelBurnTime(ItemStack stack)
    {
        // System.out.println("Checking burn time for " + stack.getItem().getRegistryName());
        if (stack.isEmpty())
        {
            return 0;
        }
        else
        {
            Item item = stack.getItem();

            if (item == ItemRegistry.TINDER)
            {
                // System.out.println("Putting tinder in fuel slot");
                return 1;
            }
            else if (item == Item.getItemFromBlock(Blocks.WOODEN_SLAB))
            {
                return 150;
            }
            else if (item == Item.getItemFromBlock(Blocks.LADDER))
            {
                return 300;
            }
            else if (item == Item.getItemFromBlock(Blocks.WOODEN_BUTTON))
            {
                return 50;
            }
            else if (Block.getBlockFromItem(item).getDefaultState().getMaterial() == Material.WOOD)
            {
                return 300;
            }
            else if (item == Item.getItemFromBlock(Blocks.COAL_BLOCK))
            {
                return 16000;
            }
            else if (item instanceof ItemTool && "WOOD".equals(((ItemTool) item).getToolMaterialName()))
            {
                return 200;
            }
            else if (item instanceof ItemSword && "WOOD".equals(((ItemSword) item).getToolMaterialName()))
            {
                return 200;
            }
            else if (item instanceof ItemHoe && "WOOD".equals(((ItemHoe) item).getMaterialName()))
            {
                return 200;
            }
            else if (item == Items.STICK)
            {
                return 50;
            }
            else if (item != Items.BOW && item != Items.FISHING_ROD)
            {
                if (item == Items.SIGN)
                {
                    return 200;
                }
                else if (item == Items.COAL)
                {
                    return 1600;
                }
                else if (item == Items.LAVA_BUCKET)
                {
                    return 20000;
                }
                else if (item != Item.getItemFromBlock(Blocks.SAPLING) && item != Items.BOWL)
                {
                    if (item == Items.BLAZE_ROD)
                    {
                        return 2400;
                    }
                    else if (item instanceof ItemDoor && item != Items.IRON_DOOR)
                    {
                        return 200;
                    }
                    else
                    {
                        return item instanceof ItemBoat ? 400 : 0;
                    }
                }
                else
                {
                    return 100;
                }
            }
            else
            {
                return 300;
            }
        }
    }

    @Override
    @Nonnull
    public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn)
    {
        return new ContainerDarkestFurnace(playerInventory, this);
    }

    public void setStokeTime(int stokeTime)
    {
        this.stokeTime = stokeTime;
    }

    private boolean isStoked()
    {
        return stokeTime > 0;
    }

    @Override
    @ParametersAreNonnullByDefault
    public boolean isItemValidForSlot(int index, ItemStack stack)
    {
        if (index == 2) // Output slot
        {
            return false;
        }
        else if (index != 1) // Input slot
        {
            return true;
        }
        else // Fuel slot
        {
            ItemStack fuelItemStack = this.furnaceItemStacks.get(SLOT_FUEL);
            // System.out.println("Checking if " + stack.getItem().getRegistryName() + " is valid for fuel slot.");

            return getFuelBurnTime(stack) > 0 || SlotFurnaceFuel.isBucket(stack) && fuelItemStack.getItem() != Items.BUCKET;
        }
    }

    @Override
    public void update()
    {
        boolean isBurningAtStart = this.isBurning();
        boolean stateHasChanged = false;

        if (this.isBurning())
        {
            --this.furnaceBurnTime;
        }

        if (this.isStoked())
        {
            --this.stokeTime;
        }

        if (!this.world.isRemote)
        {
            ItemStack fuelItem = this.furnaceItemStacks.get(SLOT_FUEL);
            // System.out.println("Stoke time: " + this.stokeTime + "; fuel item: " + fuelItem.getItem().getRegistryName());

            // If already burning, or not burning but have fuel
            if (this.isBurning() || (this.isStoked() && !fuelItem.isEmpty()))
            {
                // System.out.println("Burning, or not burning but stoked and has fuel");
                // START BURNING
                if (!this.isBurning() && this.isStoked() && !fuelItem.isEmpty())
                {
                    // System.out.println("Should start burning");
                    // Initiate burn states
                    this.furnaceBurnTime = getFuelBurnTime(fuelItem); // If returns 0, furnace won't burn
                    this.currentItemBurnTime = this.furnaceBurnTime;
                    this.stokeTime = 0;

                    if (this.isBurning()) // Fuel item had a valid burn time
                    {
                        stateHasChanged = true;

                        if (!fuelItem.isEmpty()) // Consume fuel item
                        {
                            Item item = fuelItem.getItem();
                            fuelItem.shrink(1);

                            if (fuelItem.isEmpty())
                            {
                                ItemStack item1 = item.getContainerItem(fuelItem);
                                this.furnaceItemStacks.set(SLOT_FUEL, item1);
                            }
                        }
                    }
                }

                // Increase cook time and check for completion
                if (this.isBurning() && this.canSmelt())
                {
                    ++this.cookTime;

                    if (this.cookTime == this.totalCookTime)
                    {
                        this.cookTime = 0;
                        this.totalCookTime = this.getCookTime(this.furnaceItemStacks.get(SLOT_INPUT));
                        this.smeltItem();
                        stateHasChanged = true;
                    }
                }
                else
                {
                    this.cookTime = 0;
                }
            }
            // If fuel ran out, reduce cooking progress
            else if (!this.isBurning() && this.cookTime > 0)
            {
                this.cookTime = MathHelper.clamp(this.cookTime - 2, 0, this.totalCookTime);
            }

            if (isBurningAtStart != this.isBurning())
            {
                // System.out.println("Changing furnace state to burning.");
                stateHasChanged = true;
                BlockDarkestFurnace.setState(this.isBurning(), this.world, this.pos);
            }
        }

        if (stateHasChanged)
        {
            this.markDirty();
        }
    }

    @SuppressWarnings("SimplifiableIfStatement")
    private boolean canSmelt()
    {
        if (this.furnaceItemStacks.get(SLOT_INPUT).isEmpty())
        {
            return false;
        }
        else
        {
            ItemStack smeltingResult = FurnaceRecipes.instance().getSmeltingResult(this.furnaceItemStacks.get(SLOT_INPUT));

            if (smeltingResult.isEmpty())
            {
                return false;
            }
            else
            {
                ItemStack stackInOutputSlot = this.furnaceItemStacks.get(SLOT_OUTPUT);
                int combinedSize = stackInOutputSlot.getCount() + smeltingResult.getCount();

                if (stackInOutputSlot.isEmpty())
                {
                    return true;
                }
                else if (!stackInOutputSlot.isItemEqual(smeltingResult))
                {
                    return false;
                }
                else if (combinedSize <= this.getInventoryStackLimit() && combinedSize <= stackInOutputSlot.getMaxStackSize())  // Forge fix: make furnace respect stack sizes in furnace recipes
                {
                    return true;
                }
                else
                {
                    return combinedSize <= smeltingResult.getMaxStackSize(); // Forge fix: make furnace respect stack sizes in furnace recipes
                }
            }
        }
    }
}