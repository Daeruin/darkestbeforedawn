package com.daeruin.darkestbeforedawn.utilities;

import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.primallib.config.PrimalConfig;
import com.daeruin.primallib.items.PrimalItemRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockOldLeaf;
import net.minecraft.block.BlockOldLog;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.Random;

import static net.minecraft.block.BlockPlanks.EnumType.SPRUCE;

public class DarkestUtil
{
    public static final Random random = new Random();
    private static final Material[] VALID_TORCH_SUPPORT = {Material.CLAY, Material.CRAFTED_SNOW, Material.GRASS, Material.GROUND, Material.SAND, Material.SNOW};

    public static boolean isValidTorchPlacement(World worldIn, BlockPos pos)
    {
        for (Material material : VALID_TORCH_SUPPORT)
        {
            if (worldIn.getBlockState(pos).getMaterial() == material)
            {
                return true;
            }
        }

        return false;
    }

    public static void validateTorchReplacement()
    {
        if (!DarkestConfig.basicItems.allowVanillaTorches)
        {
            String replacementBlock = DarkestConfig.basicItems.replacementForVanillaTorches;

            if (!replacementBlock.equals(""))
            {
                if (Block.getBlockFromName(replacementBlock) == null)
                {
                    System.out.println("Darkest Before Dawn: You set vanilla Minecraft torches to be replaced with '" + replacementBlock + "', which is an invalid block.");
                    System.out.println("Darkest Before Dawn: This will result in vanilla torches being deleted. Please fix your config file.");
                }
            }
        }
    }

    @Nullable
    public static Item getTorchDrop()
    {
        if (random.nextFloat() < DarkestConfig.torchBehavior.torchDropChance)
        {
            // If torches can only be crafted with shafts
            if (PrimalConfig.BRANCHES_AND_SHAFTS.allowWoodenShafts && !PrimalConfig.BRANCHES_AND_SHAFTS.sticksCountAsShafts)
            {
                return PrimalItemRegistry.WOODEN_SHAFT;
            }
            else // Torches can be crafted with sticks
            {
                return Items.STICK;
            }
        }
        else
        {
            return null;
        }
    }

    public static boolean isSpruceLeaf(IBlockState state)
    {
        return state.getBlock() == Blocks.LEAVES && state == state.withProperty(BlockOldLeaf.VARIANT, SPRUCE);
    }

    public static boolean isSpruceLog(IBlockState state)
    {
        return state.getBlock() == Blocks.LOG && state == state.withProperty(BlockOldLog.VARIANT, SPRUCE);
    }

}
