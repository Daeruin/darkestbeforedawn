package com.daeruin.darkestbeforedawn.blocks;

import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.items.ItemBlockTorchUnlit;
import com.daeruin.primallib.IHasCustomItemBlock;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Random;

public class BlockTorchUnlit extends BlockTorch implements IHasCustomItemBlock
{
    BlockTorchUnlit(String registryName)
    {
        super();
        PrimalUtilReg.initializeBlock(this, registryName);
    }

    // Turn into lit torch if clicked on with another lit torch or a vanilla torch, as long as it's not raining
    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float side, float hitX, float hitY)
    {
        if (player.getHeldItem(hand) != ItemStack.EMPTY && !world.isRainingAt(pos))
        {
            for (String itemName : DarkestConfig.torchBehavior.ignitionSourcesForBlock)
            {
                Item ignitionItem = Item.getByNameOrId(itemName);

                if (ignitionItem != null)
                {
                    Item heldItem = player.getHeldItem(hand).getItem();

                    if (heldItem == ignitionItem)
                    {
                        IBlockState priorState = world.getBlockState(pos);
                        int priorMeta = this.getMetaFromState(priorState);
                        @SuppressWarnings("deprecation")
                        IBlockState targetState = BlockRegistry.TORCH_LIT.getStateFromMeta(priorMeta); // Meta is torch's facing

                        world.setBlockState(pos, targetState, 2);
                        world.playSound(null, pos, SoundEvents.BLOCK_FIRE_AMBIENT, SoundCategory.BLOCKS, 1.0F, world.rand.nextFloat() * 0.1F + 0.9F);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    // Turn off any fire/smoke particles since this class ultimately extends vanilla BlockTorch
    @Override
    public void randomDisplayTick(IBlockState state, World world, BlockPos pos, Random rand)
    {
    }

    @Override
    public ItemBlock getItemBlock()
    {
        return new ItemBlockTorchUnlit();
    }
}
