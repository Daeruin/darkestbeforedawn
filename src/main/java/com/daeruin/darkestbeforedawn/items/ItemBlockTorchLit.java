package com.daeruin.darkestbeforedawn.items;

import com.daeruin.darkestbeforedawn.blocks.BlockRegistry;
import com.daeruin.darkestbeforedawn.blocks.BlockTileEntityTorchLit;
import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.utilities.DarkestInvUtil;
import com.daeruin.darkestbeforedawn.utilities.DarkestUtil;
import com.daeruin.primallib.util.PrimalUtilSpawn;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

public class ItemBlockTorchLit extends ItemBlock
{
    // private static final Logger LOGGER = LogManager.getLogger();

    public ItemBlockTorchLit()
    {
        super(BlockRegistry.TORCH_LIT);
        this.setMaxDamage(BlockTileEntityTorchLit.LIFESPAN); // Max damage is the number of ticks before the item is destroyed
    }

    // Only called when this ItemBlock gets placed
    // Eventually I need to allow torches to be placed in a torch bracket
    @Override
    @Nonnull
    @ParametersAreNonnullByDefault
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        if (DarkestUtil.isValidTorchPlacement(worldIn, pos))
        {
            return super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
        }

        return EnumActionResult.FAIL;
    }

    // Add 1 damage to the item every tick
    // Also drop item if not held in hand
    @Override
    public void onUpdate(ItemStack updatingStack, World world, Entity entity, int itemSlot, boolean isSelected)
    {
        // The EntityPlayerMP check ensures this only executes on the server
        if (entity instanceof EntityPlayerMP && !((EntityPlayer) entity).isCreative())
        {
            if (updatingStack.getItem() instanceof ItemBlockTorchLit && DarkestConfig.torchBehavior.torchDuration > 0)
            {
                // ItemStack#damageItem also works but makes a tool-breaking sound and doesn't replace item
                if (updatingStack.attemptDamageItem(1, world.rand, (EntityPlayerMP) entity))
                {
                    // Ensure torches going out and getting dropped happen on different ticks (so they don't interfere with each other)
                    if (world.getWorldTime() % 2 == 0)
                    {
                        @SuppressWarnings("ConstantConditions") ItemStack replacementStack = new ItemStack(DarkestUtil.getTorchDrop());

                        world.playSound(null, entity.posX, entity.posY, entity.posZ, SoundEvents.ENTITY_GENERIC_BURN, SoundCategory.NEUTRAL, 0.4F, 2.0F + DarkestUtil.random.nextFloat() * 0.4F);
                        // PrimalUtil.playSound(world, entity, SoundEvents.ENTITY_GENERIC_EXTINGUISH_FIRE);
                        DarkestInvUtil.forEachEntityInventory(
                                entity,
                                inventory -> DarkestInvUtil.tryReplaceStack(inventory, itemSlot, updatingStack, replacementStack),
                                DarkestInvUtil.EntityInventoryType.HAND,
                                DarkestInvUtil.EntityInventoryType.MAIN);
                    }
                }
            }

            // Ensure torches going out and getting dropped happen on different ticks (so they don't interfere with each other)
            if (world.getWorldTime() % 2 == 1)
            {
                // Drop torch if it isn't in main or off hand
                // Make sure updatingStack is still a torch (if destroyed, it becomes an air block and the logic below fails)
                if (updatingStack.getItem() instanceof ItemBlockTorchLit && DarkestConfig.torchBehavior.torchCanOnlyBeHeldInHand)
                {
                    ItemStack itemInMainHand = ((EntityPlayer) entity).getHeldItemMainhand();
                    ItemStack itemInOffHand = ((EntityPlayer) entity).getHeldItemOffhand();

                    // If this item (a torch) is neither in the main hand nor the off hand, expel it from the inventory
                    // If updatingStack is no longer a torch for some reason (destroyed, replaced with another item
                    if (!(updatingStack == itemInMainHand) && !(updatingStack == itemInOffHand))
                    {
                        EntityItem entityItem = new EntityItem(world, entity.posX, entity.posY, entity.posZ, updatingStack);

                        world.spawnEntity(entityItem);
                        entity.replaceItemInInventory(itemSlot, ItemStack.EMPTY);
                    }
                }
            }
        }
    }

    // Called from the EntityItem constructor when the block is broken - must override to set EntityItem's lifespan correctly
    @Override
    @SuppressWarnings("deprecation")
    public int getEntityLifespan(ItemStack itemStack, World world)
    {
        // Cannot set the EntityItem's age, so must set its lifespan instead: lifespan = item's max damage minus item's current damage level
        return itemStack.getMaxDamage() - itemStack.getItemDamage();
    }

    // Currently set to return false, because createEntity below results in an infinite loop and crashes the game

    /**
     * Determines if this Item has a special entity for when they are in the world.
     * Is called when a EntityItem is spawned in the world, if true and Item#createEntity
     * returns non null, the EntityItem will be destroyed and the new Entity will be added to the world.
     *
     * @param stack The current item stack
     * @return True of the item has a custom entity, If true, Item#createCustomEntity will be called
     */
    public boolean hasCustomEntity(ItemStack stack)
    {
        return false;
    }

    // For some reason, this results in an infinite loop and crashes the game
    // To try to enable it again, return true from hasCustomEntity above

    /**
     * This function should return a new entity to replace the dropped item.
     * Returning null here will not kill the EntityItem and will leave it to function normally.
     * Called when the item it placed in a world.
     *
     * @param world     The world object
     * @param entity    The EntityItem object, useful for getting the position of the entity
     * @param itemStack The current item stack
     * @return A new Entity object to spawn or null
     */
    @Nullable
    public Entity createEntity(World world, Entity entity, ItemStack itemStack)
    {
        if (entity instanceof EntityItem)
        {
            return new EntityItem(world, entity.posX, entity.posY, entity.posZ, itemStack);
        }
        else
        {
            return null;
        }
    }

    // For some stupid reason, when doing /give, the EntityItem starts out with an age of one less than its lifespan
    // This situation is temporary since you immediately pick the item up - if you then drop it, its age starts out at 0 like it should
    // However, it causes this code to execute, and the sound and stick/shaft to be spawned immediately
    @Override
    public boolean onEntityItemUpdate(EntityItem entityItem)
    {
        // Execute this code right before age exceeds lifespan, otherwise the EntityItem will be destroyed before this code can execute
        if (entityItem.lifespan - entityItem.getAge() == 1)
        {
            PrimalUtilSpawn.playSound(entityItem.world, entityItem, SoundEvents.ENTITY_GENERIC_EXTINGUISH_FIRE);
            PrimalUtilSpawn.spawnEntityItem(entityItem.world, entityItem.getPosition(), DarkestUtil.getTorchDrop(), 1);
        }

        return super.onEntityItemUpdate(entityItem);
    }
}
