package com.daeruin.darkestbeforedawn.client;

import com.daeruin.darkestbeforedawn.DarkestBeforeDawn;
import com.daeruin.darkestbeforedawn.inventory.ContainerCampfire;
import com.daeruin.darkestbeforedawn.tileentity.TileEntityCampfire;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiCampfire extends GuiContainer
{

    private static final ResourceLocation CAMPFIRE_GUI_TEXTURE = new ResourceLocation(DarkestBeforeDawn.MODID, "textures/gui/campfire.png");
    private final InventoryPlayer playerInventory; // The player inventory bound to this GUI
    private final TileEntityCampfire tileEntityCampfire;

    public GuiCampfire(InventoryPlayer playerInv, TileEntityCampfire tileEntityCampfire)
    {
        super(new ContainerCampfire(playerInv, tileEntityCampfire));
        this.playerInventory = playerInv;
        this.tileEntityCampfire = tileEntityCampfire;
    }

    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        String s = "Campfire";
        this.fontRenderer.drawString(s, this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, 6, 4210752);
        this.fontRenderer.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, this.ySize - 96 + 2, 4210752);
    }

    /**
     * Draws the background layer of this container (behind the items).
     */
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(CAMPFIRE_GUI_TEXTURE);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;

        // Render main GUI
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

        // Render flame progress
        if (TileEntityCampfire.isBurning(this.tileEntityCampfire))
        {
            int k = this.getRemainingBurnScaled();
            this.drawTexturedModalRect(i + 56, j + 36 + 12 - k, 176, 12 - k, 14, k + 1);
        }

        // Render white arrow progress
        int l = this.getCookProgressScaled();
        this.drawTexturedModalRect(i + 79, j + 34, 176, 14, l + 1, 16);
    }

    private int getCookProgressScaled()
    {
        int i = this.tileEntityCampfire.getField(TileEntityCampfire.FIELD_COOK_TIME);
        int j = this.tileEntityCampfire.getField(TileEntityCampfire.FIELD_TOTAL_COOK_TIME);
        return j != 0 && i != 0 ? i * 24 / j : 0;
    }

    private int getRemainingBurnScaled()
    {
        int i = this.tileEntityCampfire.getField(TileEntityCampfire.FIELD_BURN_TIME_INITIAL);

        if (i == 0)
        {
            i = 200;
        }

        return this.tileEntityCampfire.getField(TileEntityCampfire.FIELD_BURN_TIME_REMAINING) * 13 / i;
    }

}
