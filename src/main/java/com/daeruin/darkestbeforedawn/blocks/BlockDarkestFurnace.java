package com.daeruin.darkestbeforedawn.blocks;

import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.items.ItemBlockTorchLit;
import com.daeruin.darkestbeforedawn.items.ItemRegistry;
import com.daeruin.darkestbeforedawn.tileentity.TileEntityDarkestFurnace;
import com.daeruin.primallib.util.PrimalUtilReg;
import com.daeruin.primallib.util.PrimalUtilSpawn;
import net.minecraft.block.BlockFurnace;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import javax.annotation.ParametersAreNonnullByDefault;

public class BlockDarkestFurnace extends BlockFurnace
{
    @SuppressWarnings("unused")
    private static boolean keepInventory;

    BlockDarkestFurnace(String registryName, boolean isBurning)
    {
        super(isBurning);
        PrimalUtilReg.initializeBlock(this, registryName);
    }

    @Override
    @ParametersAreNonnullByDefault
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        if (worldIn.isRemote)
        {
            return true;
        }
        else
        {
            TileEntity tileentity = worldIn.getTileEntity(pos);

            if (tileentity instanceof TileEntityDarkestFurnace)
            {
                Item heldItem = playerIn.getHeldItem(hand).getItem();
                // System.out.println("TileEntityDarkestFurnace is here. Clicking with: " + heldItem.getRegistryName());

                if (heldItem instanceof ItemBlockTorchLit || (DarkestConfig.basicItems.allowVanillaTorches && heldItem == Item.getItemFromBlock(Blocks.TORCH)))
                {
                    // System.out.println("Clicking furnace with lit torch!");
                    if (tileentity.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null))
                    {
                        // System.out.println("Tile entity has capability: " + tileentity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null));
                        IItemHandler itemHandler = tileentity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
                        assert itemHandler != null;

                        if (itemHandler.getStackInSlot(1).getItem() == ItemRegistry.TINDER)
                        {
                            // System.out.println("Fuel slot had: " + itemHandler.getStackInSlot(1) + ". Furnace is stoked!");
                            itemHandler.extractItem(1, 1, false);
                            ((TileEntityDarkestFurnace) tileentity).setStokeTime(200);
                            PrimalUtilSpawn.spawnFlame(worldIn, pos.getX(), pos.getY(), pos.getZ());
                        }
                    }
                }

                // playerIn.openGui(DarkestBeforeDawn.INSTANCE, GuiHandler.GUI_FURNACE, worldIn, pos.getX(), pos.getY(), pos.getZ());
                playerIn.addStat(StatList.FURNACE_INTERACTION);
            }

            return true;
        }
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta)
    {
        return new TileEntityDarkestFurnace();
    }

    @Override
    @ParametersAreNonnullByDefault
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
    {
        worldIn.setBlockState(pos, state.withProperty(FACING, placer.getHorizontalFacing().getOpposite()), 2);

        if (stack.hasDisplayName())
        {
            TileEntity tileentity = worldIn.getTileEntity(pos);

            if (tileentity instanceof TileEntityDarkestFurnace)
            {
                ((TileEntityDarkestFurnace) tileentity).setCustomInventoryName(stack.getDisplayName());
            }
        }
    }

    @Override
    @ParametersAreNonnullByDefault
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        if (!keepInventory)
        {
            TileEntity tileentity = worldIn.getTileEntity(pos);

            if (tileentity instanceof TileEntityDarkestFurnace)
            {
                InventoryHelper.dropInventoryItems(worldIn, pos, (TileEntityDarkestFurnace) tileentity);
                worldIn.updateComparatorOutputLevel(pos, this);
            }
        }

        super.breakBlock(worldIn, pos, state);
    }
}
