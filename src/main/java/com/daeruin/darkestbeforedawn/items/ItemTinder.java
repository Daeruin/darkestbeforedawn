package com.daeruin.darkestbeforedawn.items;

import com.daeruin.darkestbeforedawn.blocks.BlockRegistry;
import com.daeruin.darkestbeforedawn.blocks.BlockTileEntityCampfire;
import com.daeruin.darkestbeforedawn.blocks.BlockTileEntityCampfire.EnumCampfireState;
import com.daeruin.primallib.items.ItemBase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nonnull;

public class ItemTinder extends ItemBase
{
    ItemTinder(String registryName)
    {
        super(registryName);
    }

    @Override
    @Nonnull
    public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        for (Material material : BlockTileEntityCampfire.VALID_CAMPFIRE_SUPPORT)
        {
            IBlockState stateClickedOn = worldIn.getBlockState(pos);

            if (stateClickedOn.getMaterial() == material // If material at the destination is valid campfire support
                    // && worldIn.getBlockState(pos.up()).getBlock() == Blocks.AIR // Block immediately above must be air
                    && facing == EnumFacing.UP // Can only place campfire on top surfaces
                    && stateClickedOn.getBlockFaceShape(worldIn, pos, facing) == BlockFaceShape.SOLID) // Can only place campfire on solid surfaces
            {
                // System.out.println("Placing tinder");
                playerIn.getHeldItem(hand).shrink(1);
                worldIn.setBlockState(pos.up(), BlockRegistry.CAMPFIRE.getDefaultState().withProperty(BlockTileEntityCampfire.CAMPFIRE_STATE, EnumCampfireState.TINDER));

                Block block = stateClickedOn.getBlock();

                if (block == Blocks.FARMLAND || block == Blocks.GRASS_PATH)
                {
                    worldIn.setBlockState(pos, Blocks.DIRT.getDefaultState());
                }

                return EnumActionResult.PASS; // Seems to work with either PASS or SUCCESS
            }
        }
        return EnumActionResult.FAIL;
    }
}
