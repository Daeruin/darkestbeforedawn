package com.daeruin.darkestbeforedawn.blocks;

import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.items.ItemBlockTorchLit;
import com.daeruin.darkestbeforedawn.items.ItemBlockTorchUnlit;
import com.daeruin.darkestbeforedawn.tileentity.TileEntityTorch;
import com.daeruin.primallib.IHasCustomItemBlock;
import com.daeruin.primallib.config.PrimalConfig;
import com.daeruin.primallib.items.PrimalItemRegistry;
import com.daeruin.primallib.util.PrimalUtilReg;
import com.daeruin.primallib.util.PrimalUtilSpawn;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Random;

@SuppressWarnings("unused")
public class BlockTileEntityTorchLit extends BlockTorch implements IHasCustomItemBlock
{

    public static final int LIFESPAN = DarkestConfig.torchBehavior.torchDuration;
    private static final int SMOLDERING = (int) (LIFESPAN * DarkestConfig.torchBehavior.smolderingRatio);
    private static final int SMOLDERING_LIGHT_VALUE = DarkestConfig.torchBehavior.smolderingBrightness;
    private static final int NORMAL_LIGHT_VALUE = DarkestConfig.torchBehavior.fullBrightness;
    protected String registryName;

    BlockTileEntityTorchLit(String registryName)
    {
        super();
        PrimalUtilReg.initializeBlock(this, registryName);
    }

    @SuppressWarnings("SameReturnValue")
    public int getLifespan()
    {
        return LIFESPAN;
    }

    private boolean isSmoldering(IBlockAccess world, BlockPos pos)
    {
        // TileEntityTorch tileEntity = (TileEntityTorch) world.getTileEntity(pos);
        TileEntity tileEntity = world.getTileEntity(pos);

        return tileEntity != null && tileEntity instanceof TileEntityTorch && (((TileEntityTorch) tileEntity).currentAge > SMOLDERING);

    }

    // If torch is smoldering, diminish light value
    @Override
    public int getLightValue(IBlockState state, IBlockAccess world, BlockPos pos)
    {
        if (isSmoldering(world, pos))
        {
            return SMOLDERING_LIGHT_VALUE;
        }
        else
        {
            return NORMAL_LIGHT_VALUE;
        }
    }

    // If torch is smoldering, only spawn flame 25% of the time
    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(IBlockState state, World world, BlockPos pos, Random rand)
    {
        if (isSmoldering(world, pos))
        {
            final EnumFacing facing = state.getValue(FACING);
            final double x = (double) pos.getX() + 0.5D;
            final double y = (double) pos.getY() + 0.7D;
            final double z = (double) pos.getZ() + 0.5D;
            final double mod1 = 0.22D;
            final double mod2 = 0.27D;
            final int r = rand.nextInt(4);

            if (facing.getAxis().isHorizontal())
            {
                final EnumFacing opposite = facing.getOpposite();
                world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, x + mod2 * (double) opposite.getXOffset(), y + mod1, z + mod2 * (double) opposite.getZOffset(), 0.0D, 0.0D, 0.0D);
                if (r == 2)
                {
                    world.spawnParticle(EnumParticleTypes.FLAME, x + mod2 * (double) opposite.getXOffset(), y + mod1, z + mod2 * (double) opposite.getZOffset(), 0.0D, 0.0D, 0.0D);
                }
            }
            else
            {
                world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, x, y, z, 0.0D, 0.0D, 0.0D);
                if (r == 2)
                {
                    world.spawnParticle(EnumParticleTypes.FLAME, x, y, z, 0.0D, 0.0D, 0.0D);
                }
            }
        }
        else
        {
            super.randomDisplayTick(state, world, pos, rand);
        }
    }

    // Keep track of torch's lifespan by copying item's damage to tile entity's currentAge
    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
    {
        TileEntityTorch tileEntity = (TileEntityTorch) worldIn.getTileEntity(pos);

        if (tileEntity != null)
        {
            tileEntity.currentAge = stack.getItemDamage();
        }

        super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
    }


    // On shift-right-click, pick up torch
    // Consider extending BlockBase instead of BlockTorch, to use BlockBase's onBlockActivated code
    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if (!world.isRemote)
        {
            if (DarkestConfig.torchBehavior.sneakToPickUp && player.isSneaking())
            {
                TileEntity tileEntity = world.getTileEntity(pos);

                spawnTorchEntity(world, pos, tileEntity);

                return true;
            }
            else
            {
                if (player.getHeldItem(hand).getItem() instanceof ItemBlockTorchUnlit)
                {
                    return false; // This allows ItemBlockTorchUnlit#onItemUse to run, so the unlit torch will be lit
                }
            }
        }

        return false;
    }


    private void spawnTorchEntity(@Nonnull World world, @Nonnull BlockPos pos, TileEntity tileEntity)
    {
        // Tip: Don't try to create a new PrimalItemBlockTorchLit, because it won't have a registryName.
        ItemStack itemStack = new ItemStack(Item.getItemFromBlock(this));
        int blockTorchAge;

        if (DarkestConfig.torchBehavior.torchDuration > 0 && tileEntity != null && tileEntity instanceof TileEntityTorch)
        {
            blockTorchAge = ((TileEntityTorch) tileEntity).currentAge;

            if (blockTorchAge > 0)
            {
                itemStack.setItemDamage(blockTorchAge);
            }
        }

        EntityItem entityItem = new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), itemStack);

        world.spawnEntity(entityItem);
        world.setBlockToAir(pos);
    }

    // Delay deletion of block until after harvestBlock, so tile entity isn't deleted yet
    @Override
    @ParametersAreNonnullByDefault
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest)
    {
        if (willHarvest)
            return true; // If it will harvest, delay deletion of the block until after harvestBlock
        //noinspection ConstantConditions
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    // Spawn new torch item with proper amount of damage based on tile entity's currentAge
    @Override
    public void harvestBlock(@Nonnull World world, EntityPlayer player, @Nonnull BlockPos pos, @Nonnull IBlockState state, TileEntity tileEntity, ItemStack stack)
    {
        if (DarkestConfig.torchBehavior.sneakToPickUp) // If true, player must sneak right-click to pick up; left clicking destroys torch and drops shaft/stick
        {
            // If torches can only be crafted with shafts
            if (PrimalConfig.BRANCHES_AND_SHAFTS.allowWoodenShafts && !PrimalConfig.BRANCHES_AND_SHAFTS.sticksCountAsShafts)
            {
                PrimalUtilSpawn.spawnEntityItem(world, pos, PrimalItemRegistry.WOODEN_SHAFT, 1);
            }
            else // Torches can be crafted with sticks
            {
                PrimalUtilSpawn.spawnEntityItem(world, pos, Items.STICK, 1);
            }
        }
        else
        {
            spawnTorchEntity(world, pos, tileEntity);
        }

        world.setBlockToAir(pos);
    }

    @SuppressWarnings("unused")
    public Class<TileEntityTorch> getTileEntityClass()
    {
        return TileEntityTorch.class;
    }

    public TileEntityTorch getTileEntity(IBlockAccess world, BlockPos pos)
    {
        return (TileEntityTorch) world.getTileEntity(pos);
    }

    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    @Override
    @ParametersAreNonnullByDefault
    public TileEntityTorch createTileEntity(World world, IBlockState state)
    {
        return new TileEntityTorch();
    }

    @Override
    public ItemBlock getItemBlock()
    {
        return new ItemBlockTorchLit();
    }
}
