package com.daeruin.darkestbeforedawn.blocks;

import com.daeruin.primallib.blocks.BlockBase;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;

public class BlockFireboard extends BlockBase
{
    private final AxisAlignedBB AABB;

    BlockFireboard(String registryName)
    {
        super(registryName, Material.WOOD, true);
        this.setHardness(1.0F);
        this.setResistance(0.0F);
        this.AABB = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.09375D, 0.9375D);
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @SuppressWarnings("deprecation")
    @Override
    @Nonnull
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return this.AABB;
    }

    // Allows block texture's transparency to work - otherwise it's filled with black
    @SideOnly(Side.CLIENT)
    @Override
    @Nonnull
    public BlockRenderLayer getRenderLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    // @Override
    // public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    // {
    //     // The if-statement is a copy from PrimalBlockBase; needed because the method needs to return false if the if-statement fails (see below)
    //     if (!world.isRemote)
    //     {
    //         if (player.isSneaking() && canBePickedUp())
    //         {
    //             spawnAsEntity(world, pos, new ItemStack(this));
    //             world.setBlockToAir(pos);
    //         }
    //     }
    //
    //     return false; // Allows shaft to be used on fireboards, as long as the above code fails (picking up fireboard trumps making a fire)
    // }

    @Override
    public boolean useBlock(World world, BlockPos pos, EntityPlayer player)
    {
        return false;
    }
}
