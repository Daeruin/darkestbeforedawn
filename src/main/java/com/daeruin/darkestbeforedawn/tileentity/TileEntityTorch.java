package com.daeruin.darkestbeforedawn.tileentity;

import com.daeruin.darkestbeforedawn.blocks.BlockTileEntityTorchLit;
import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.utilities.DarkestUtil;
import com.daeruin.primallib.util.PrimalUtilSpawn;
import net.minecraft.block.Block;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TileEntityTorch extends TileEntity implements ITickable
{

    public int currentAge = 0; // How long the torch will burn

    public TileEntityTorch()
    {
    }

    // Delete torch if it runs out of fuel
    @Override
    public void update() // Needs to occur on client and server to keep data synced
    {
        if (DarkestConfig.torchBehavior.torchDuration > 0)
        {
            World world = this.getWorld();
            Block block = world.getBlockState(this.pos).getBlock();

            currentAge++;
            world.checkLightFor(EnumSkyBlock.BLOCK, pos); // Check if light level needs to be updated

            if (block instanceof BlockTileEntityTorchLit) // Check if torch has reached its lifespan and needs to be deleted
            {
                int lifespan = ((BlockTileEntityTorchLit) block).getLifespan();

                if (currentAge > lifespan)
                {
                    world.setBlockToAir(this.pos);

                    if (!world.isRemote) // Only spawn items on server (spawning on client results in ghost items)
                    {
                        world.playSound(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), SoundEvents.ENTITY_GENERIC_EXTINGUISH_FIRE, SoundCategory.BLOCKS, 0.3F, 1.0F);
                        PrimalUtilSpawn.spawnEntityItem(world, this.pos, DarkestUtil.getTorchDrop(), 1);
                    }
                }
            }
        }
    }

    // Saves and reads currentAge value when loading and saving the world

    @Override
    @Nonnull
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        compound.setInteger("Counter", this.currentAge);
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        this.currentAge = compound.getInteger("Counter");
        super.readFromNBT(compound);
    }

    // Required to sync currentAge value between client and server

    @Override
    @Nullable
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(this.pos, 3, this.getUpdateTag());
    }

    @Override
    @Nonnull
    public NBTTagCompound getUpdateTag()
    {
        return this.writeToNBT(new NBTTagCompound());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
        super.onDataPacket(net, pkt);
        handleUpdateTag(pkt.getNbtCompound());
    }
}
