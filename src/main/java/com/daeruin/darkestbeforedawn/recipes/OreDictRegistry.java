package com.daeruin.darkestbeforedawn.recipes;

import com.daeruin.darkestbeforedawn.blocks.BlockRegistry;
import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.items.ItemRegistry;
import com.daeruin.primallib.items.PrimalItemRegistry;
import com.daeruin.primallib.util.PrimalUtilRecipe;
import net.minecraft.item.ItemStack;

public class OreDictRegistry
{
    public static void registerOreDictEntries()
    {
        if (DarkestConfig.basicItems.allowAnimalFat)
        {
            PrimalUtilRecipe.addAlternateIngredients("fat", ItemRegistry.ANIMAL_FAT_RAW, ItemRegistry.CLAY_BOWL_FAT_RENDERED);
            PrimalUtilRecipe.addAlternateIngredients("torchFuel", new ItemStack(ItemRegistry.CLAY_BOWL_FAT_RENDERED, 1, 32767));
        }

        if (DarkestConfig.basicItems.allowResin)
        {
            PrimalUtilRecipe.addAlternateIngredients("sap", ItemRegistry.RESIN, ItemRegistry.CLAY_BOWL_RESIN_MELTED);
            PrimalUtilRecipe.addAlternateIngredients("resin", ItemRegistry.RESIN, ItemRegistry.CLAY_BOWL_RESIN_MELTED);
            PrimalUtilRecipe.addAlternateIngredients("torchFuel", ItemRegistry.CLAY_BOWL_RESIN_MELTED);
            PrimalUtilRecipe.addAlternateIngredients("logWood", BlockRegistry.LOG_SPRUCE_RESINOUS);
        }

        if (DarkestConfig.basicItems.allowBirchBarkTorches || DarkestConfig.basicItems.allowAnyBark)
        {
            PrimalUtilRecipe.addAlternateIngredients("torchHead", PrimalItemRegistry.BIRCH_BARK);
        }

        if (DarkestConfig.basicItems.allowAnyBark)
        {
            PrimalUtilRecipe.addAlternateIngredients("torchHead", PrimalItemRegistry.ACACIA_BARK);
            PrimalUtilRecipe.addAlternateIngredients("torchHead", PrimalItemRegistry.DARK_OAK_BARK);
            PrimalUtilRecipe.addAlternateIngredients("torchHead", PrimalItemRegistry.JUNGLE_BARK);
            PrimalUtilRecipe.addAlternateIngredients("torchHead", PrimalItemRegistry.OAK_BARK);
            PrimalUtilRecipe.addAlternateIngredients("torchHead", PrimalItemRegistry.SPRUCE_BARK);
        }

        if (DarkestConfig.basicItems.allowSpruceConeTorches)
        {
            PrimalUtilRecipe.addAlternateIngredients("torchHead", ItemRegistry.SPRUCE_CONE);
        }
    }
}
