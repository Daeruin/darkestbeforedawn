package com.daeruin.darkestbeforedawn.inventory;

import com.daeruin.darkestbeforedawn.recipes.CampfireRecipes;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

class SlotCampfireOutput extends SlotItemHandler
{

    private final EntityPlayer player;
    private int removeCount;

    SlotCampfireOutput(EntityPlayer player, IItemHandler itemHandler, @SuppressWarnings("SameParameterValue") int slotIndex, @SuppressWarnings("SameParameterValue") int xPosition, @SuppressWarnings("SameParameterValue") int yPosition)
    {
        super(itemHandler, slotIndex, xPosition, yPosition);
        this.player = player;
    }

    // Check if the ItemStack is allowed to be placed in a slot. Because this is the output slot, nothing can be placed in it.
    // This method makes it so we need no override for getSlotStackLimit, because items can't be placed in this slot anyway.
    // The tile entity enforces only 1 thing at a time in the output slot via the canSmelt check.
    @Override
    @ParametersAreNonnullByDefault
    public boolean isItemValid(ItemStack stack)
    {
        return false;
    }

    // Decrease the size of the ItemStack by the given amount. Returns the new ItemStack.
    @Override
    @Nonnull
    public ItemStack decrStackSize(int amount)
    {
        if (this.getHasStack())
        {
            this.removeCount += Math.min(amount, this.getStack().getCount());
        }

        return super.decrStackSize(amount);
    }

    // The ItemStack passed in is the output
    protected void onCrafting(ItemStack stack, int amount)
    {
        this.removeCount += amount;
        this.onCrafting(stack);
    }

    // Generates XP orbs and achievements. The ItemStack passed in is the output.
    protected void onCrafting(ItemStack stack)
    {
        stack.onCrafting(this.player.world, this.player, this.removeCount);

        if (!this.player.world.isRemote)
        {
            int i = this.removeCount;
            float xp = CampfireRecipes.instance().getCookingExperience(stack);

            if (xp == 0.0F)
            {
                i = 0;
            }
            else if (xp < 1.0F)
            {
                int j = MathHelper.floor((float) i * xp);

                if (j < MathHelper.ceil((float) i * xp) && Math.random() < (double) ((float) i * xp - (float) j))
                {
                    ++j;
                }

                i = j;
            }

            while (i > 0)
            {
                int k = EntityXPOrb.getXPSplit(i);
                i -= k;
                this.player.world.spawnEntity(new EntityXPOrb(this.player.world, this.player.posX, this.player.posY + 0.5D, this.player.posZ + 0.5D, k));
            }
        }

        this.removeCount = 0;

        net.minecraftforge.fml.common.FMLCommonHandler.instance().firePlayerSmeltedEvent(player, stack);
    }

}
