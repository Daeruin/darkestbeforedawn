package com.daeruin.darkestbeforedawn.blocks;

import com.daeruin.darkestbeforedawn.DarkestBeforeDawn;
import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.items.ItemRegistry;
import com.daeruin.darkestbeforedawn.network.CampfirePacket;
import com.daeruin.darkestbeforedawn.network.GuiHandler;
import com.daeruin.darkestbeforedawn.network.PacketHandler;
import com.daeruin.darkestbeforedawn.tileentity.TileEntityCampfire;
import com.daeruin.primallib.config.PrimalConfig;
import com.daeruin.primallib.items.PrimalItemRegistry;
import com.daeruin.primallib.util.PrimalUtil;
import com.daeruin.primallib.util.PrimalUtilSpawn;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Random;

// Need to make this extend PrimalLib#BlockTileEntity instead
@SuppressWarnings("deprecation")
public class BlockTileEntityCampfire extends BlockTileEntity<TileEntityCampfire>
{

    public static final PropertyEnum<EnumCampfireState> CAMPFIRE_STATE = PropertyEnum.create("campfire_state", BlockTileEntityCampfire.EnumCampfireState.class);
    public static final Material[] VALID_CAMPFIRE_SUPPORT = {Material.CLAY, Material.GROUND, Material.IRON, Material.ROCK, Material.SAND};
    private static final int STOKE_THRESHOLD = 80;
    private static final int MAX_STOKE_TIME = 140;
    private static final int TINDER_MIN_BREATHS = 3;
    private static final int TINDER_MAX_BREATHS = 5;
    private static final int KINDLING_MIN_BREATHS = 6;
    private static final int KINDLING_MAX_BREATHS = 8;
    private static final int NUM_TWIGS_FOR_KINDLING = 3;
    private static final AxisAlignedBB AABB_BURNING = new AxisAlignedBB(0.125D, 0.0D, 0.125D, 0.875D, 0.9375D, 0.875D);
    private static final AxisAlignedBB AABB_DEFAULT = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.09375D, 0.9375D);

    BlockTileEntityCampfire(String registryName)
    {
        super(registryName, DarkestBeforeDawn.PRIMAL_CAMPFIRE, 0.0F, 0.0F, false);
        this.setDefaultState(this.blockState.getBaseState().withProperty(CAMPFIRE_STATE, BlockTileEntityCampfire.EnumCampfireState.TINDER));
    }

    // Helper method to change the campfire's state
    public static void setCampfireState(World world, BlockPos pos, EnumCampfireState campfireState)
    {
        // System.out.println("Setting campfire to state: " + campfireState.name);
        world.setBlockState(pos, BlockRegistry.CAMPFIRE.getDefaultState().withProperty(BlockTileEntityCampfire.CAMPFIRE_STATE, campfireState));
    }

    @Override
    public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entityIn)
    {
        if (state.getValue(CAMPFIRE_STATE) == EnumCampfireState.BURNING)
            entityIn.attackEntityFrom(DamageSource.ON_FIRE, 1.0F);
    }

    @Override
    @Nonnull
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        if (state.getValue(CAMPFIRE_STATE) == EnumCampfireState.BURNING)
            return AABB_BURNING;
        else
            return AABB_DEFAULT;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState state, @Nullable IBlockAccess world, @Nullable BlockPos pos)
    {
        if (state.getValue(CAMPFIRE_STATE) == EnumCampfireState.BURNING)
            return AABB_BURNING;
        else
            return NULL_AABB;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    public int getLightValue(IBlockState state)
    {
        switch (state.getValue(CAMPFIRE_STATE))
        {
            case TINDER:
            case KINDLING:
            case ASHES:
                return 0;
            case TINDER_STOKED:
                return 2;
            case KINDLING_STOKED:
            case COALS:
                return 5;
            case BURNING:
                return 14;
        }
        return 0;
    }

    // Allows block texture's transparency to work - otherwise it's filled with black
    @SideOnly(Side.CLIENT)
    @Override
    @Nonnull
    public BlockRenderLayer getRenderLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    /**
     * The type of render function called. MODEL for mixed TESR and static model, MODELBLOCK_ANIMATED for TESR-only,
     * LIQUID for vanilla liquids, INVISIBLE to skip all rendering
     */
    @Override
    @Nonnull
    public EnumBlockRenderType getRenderType(IBlockState state)
    {
        return EnumBlockRenderType.MODEL;
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
    {
        if (worldIn.getBlockState(pos.down()).getBlock() == Blocks.GRASS)
        {
            // System.out.println("Setting block below to dirt");
            worldIn.setBlockState(pos.down(), Blocks.DIRT.getDefaultState());
        }
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
        this.checkAndDropBlock(worldIn, pos, state);
    }

    @Override
    public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
    {
        this.checkAndDropBlock(worldIn, pos, state);
    }

    private void checkAndDropBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        if (!this.canBlockStay(worldIn, pos, state))
        {
            this.dropBlockAsItem(worldIn, pos, state, 0); // Eventually leads to getDrops, which is overridden below
            worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
        }
    }

    // pos is the position of the campfire block
    private boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state)
    {
        // System.out.println("Can block stay at " + pos + "?");
        // System.out.println("Is this block itself? " + (state.getBlock() == this));
        if (state.getBlock() == this) //Forge: This function is called during world gen and placement, before this block is set, so if we are not 'here' then assume it's the pre-check.
        {
            IBlockState stateBelow = worldIn.getBlockState(pos.down());
            // System.out.println("Block below: " + stateBelow + "; Face shape below: " + stateBelow.getBlockFaceShape(worldIn, pos.down(), EnumFacing.UP));

            for (Material material : BlockTileEntityCampfire.VALID_CAMPFIRE_SUPPORT)
            {
                if (stateBelow.getMaterial() == material // Campfire must be sitting on a valid support material
                        && stateBelow.getBlockFaceShape(worldIn, pos.down(), EnumFacing.UP) == BlockFaceShape.SOLID) // Campfire must be on a solid surface
                {
                    // System.out.println("Block can stay");
                    return true;
                }
            }
        }
        // System.out.println("Block cannot stay");
        return false;
    }

    // When block is broken expel any items in its slots
    // Called server-side after this block is replaced with another in Chunk, but before the Tile Entity is updated
    @ParametersAreNonnullByDefault
    public void breakBlock(World world, BlockPos pos, IBlockState state)
    {
        // System.out.println("BreakBlock - dropping inventory");
        dropInventory(world, pos);

        super.breakBlock(world, pos, state);
    }

    @Override
    public void getDrops(@Nonnull NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, @Nonnull IBlockState state, int fortune)
    {
        // System.out.println("Getting drops");
        switch (state.getValue(CAMPFIRE_STATE))
        {
            case TINDER:
            case TINDER_STOKED:
                drops.add(new ItemStack(ItemRegistry.TINDER, 1));
                break;
            case KINDLING:
            case KINDLING_STOKED:
                drops.add(new ItemStack(ItemRegistry.TINDER, 1));
                if (PrimalConfig.TWIGS.allowTwigs)
                    drops.add(new ItemStack(PrimalItemRegistry.TWIG_GENERIC, 3));
                break;
            case ASHES:
                if (DarkestConfig.basicItems.allowCharcoalDust)
                    drops.add(new ItemStack(ItemRegistry.DUST_CHARCOAL, 1));
                break;
            case COALS:
            case BURNING:
                dropInventory(world, pos);
                if (DarkestConfig.basicItems.allowCharcoalDust)
                    drops.add(new ItemStack(ItemRegistry.DUST_CHARCOAL, 1));
                break;
        }
        // System.out.println("Drops for " + state.getValue(CAMPFIRE_STATE).name + ": " + drops);
    }

    // Calls the TileEntityCampfire's custom method for expelling its inventory
    // This changes the actual inventory's contents, so this method can be called multiple times, and the inventory will only be expelled once (unless the inventory changes in between calls, obviously)
    private void dropInventory(IBlockAccess world, BlockPos pos)
    {
        TileEntityCampfire tileEntity = getTileEntity(world, pos);

        if (tileEntity != null)
        {
            tileEntity.expelInventory();
        }
    }

    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(IBlockState stateIn, World world, BlockPos pos, Random rand)
    {
        // BlockPos is always one corner of the block; add +0.5D to all coordinates to get to the center
        double yPos = (double) pos.getY();
        double xPos = (double) pos.getX();
        double zPos = (double) pos.getZ();
        double yAdj = rand.nextDouble();
        double xAdj = rand.nextDouble();
        double zAdj = rand.nextDouble();

        EnumCampfireState campfireState = stateIn.getValue(CAMPFIRE_STATE);

        if (campfireState == EnumCampfireState.TINDER_STOKED
                || campfireState == EnumCampfireState.KINDLING_STOKED
                || campfireState == EnumCampfireState.COALS)
        {
            // Particles should spawn in a small square near the center of the block floor
            xAdj = xAdj * 0.5D + 0.25D;
            zAdj = zAdj * 0.5D + 0.25D;
            PrimalUtilSpawn.spawnLittleSmoke(world, xPos + xAdj, yPos, zPos + zAdj);
        }

        if (campfireState == EnumCampfireState.BURNING)
        {
            // Particles should spawn within a cone inside the block; the cone's max height is at 0.6;
            yAdj = yAdj * 0.6D; // Adjust particle height down by 60%

            // If X or Z are below the lower bound set by the Y coord, bring it within
            // This should cause flame to appear along the outside of the cone more often, and near the tip of the cone more often
            double lowerBound = yAdj / 2;
            double upperBound = 100 - (yAdj / 2);
            if (xAdj < lowerBound)
                xAdj = lowerBound;
            if (xAdj > upperBound)
                xAdj = upperBound;
            if (zAdj < lowerBound)
                zAdj = lowerBound;
            if (zAdj > upperBound)
                zAdj = upperBound;

            PrimalUtilSpawn.spawnFlame(world, xPos + xAdj, yPos + yAdj, zPos + zAdj);
            PrimalUtilSpawn.spawnBigSmoke(world, xPos + 0.5D, yPos + yAdj, zPos + 0.5D);
            PrimalUtilSpawn.playFireAudio(world, xPos + 0.5D, yPos + 0.5D, zPos + 0.5D);
        }
    }

    /*
     * This method handles block state changes that occur when the player interacts with it.
     * TileEntityCampfire#update handles changes due to automatic processes such as running out of fuel.
     */
    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if (!world.isRemote) // server
        {

            TileEntityCampfire tileEntity = getTileEntity(world, pos);

            if (tileEntity == null)
                return false;

            EnumCampfireState campfireState = state.getValue(CAMPFIRE_STATE);
            ItemStack heldItemStack = player.getHeldItem(hand);
            Item heldItem = heldItemStack.getItem();
            // System.out.println("Held item: " + heldItem);

            // TINDER
            if (campfireState == EnumCampfireState.TINDER)
            {
                // System.out.println("TINDER");
                // System.out.println("(Before) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
                if (!heldItemStack.isEmpty())
                {
                    if (heldItem == ItemRegistry.GLOWING_EMBER
                            || heldItem == Items.FLINT_AND_STEEL
                            || DarkestConfig.basicItems.allowUnlitTorches && heldItem == Item.getItemFromBlock(BlockRegistry.TORCH_LIT)
                            || (DarkestConfig.basicItems.allowVanillaTorches && heldItem == Item.getItemFromBlock(Blocks.TORCH))
                            && !world.isRainingAt(pos.up()))
                    {
                        // System.out.println("Holding ember or flint/steel or torch");
                        if (player.isCreative())
                        {
                            tileEntity.setBurnTimeRemaining(4000);
                            tileEntity.setBurnTimeInitial(4000);
                            tileEntity.setCoalsCoolDown(600);
                            setCampfireState(world, pos, EnumCampfireState.BURNING);
                            return true; // Don't open GUI or place held block
                        }
                        else
                        {
                            if (heldItem == ItemRegistry.GLOWING_EMBER)
                                heldItemStack.shrink(1);
                            else if (heldItem == Items.FLINT_AND_STEEL)
                                heldItemStack.damageItem(1, player);

                            if (tileEntity.getStokeTimer() == 0 && !world.isRainingAt(pos.up()))
                            {
                                tileEntity.setStokeTimer(STOKE_THRESHOLD);
                                setCampfireState(world, pos, EnumCampfireState.TINDER_STOKED);
                            }
                        }
                    }
                    // Part of shortcut to stoked kindling if you already have a burning torch
                    else if (heldItem == PrimalItemRegistry.TWIG_GENERIC && heldItemStack.getCount() >= NUM_TWIGS_FOR_KINDLING)
                    {
                        // System.out.println("Holding twigs");
                        heldItemStack.shrink(NUM_TWIGS_FOR_KINDLING);
                        tileEntity.setStokeTimer(0);
                        setCampfireState(world, pos, EnumCampfireState.KINDLING);
                    }
                }
                else if (player.isSneaking()) // Holding nothing
                {
                    // System.out.println("Pick up tinder");
                    // Pick up the tinder
                    spawnAsEntity(world, pos, new ItemStack(ItemRegistry.TINDER));
                    world.setBlockToAir(pos);
                }
                // System.out.println("(After) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
            }
            // STOKED TINDER
            // Note: Stoked tinder always spawns small smoke particles in randomDisplayTick
            else if (campfireState == EnumCampfireState.TINDER_STOKED)
            {
                // System.out.println("STOKED TINDER");
                // System.out.println("(Before) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
                if (!heldItemStack.isEmpty())
                {
                    if (heldItem == PrimalItemRegistry.TWIG_GENERIC && heldItemStack.getCount() >= NUM_TWIGS_FOR_KINDLING)
                    {
                        // System.out.println("Holding twigs");
                        heldItemStack.shrink(NUM_TWIGS_FOR_KINDLING);

                        if (tileEntity.getTimesBlown() >= TINDER_MIN_BREATHS)
                        {
                            tileEntity.setStokeTimer(tileEntity.getStokeTimer() - 40); // Adding twigs partially smothers the flames; need to blow sooner
                            setCampfireState(world, pos, EnumCampfireState.KINDLING_STOKED);
                        }
                        else // timesBlown is not high enough
                        {
                            tileEntity.setStokeTimer(0); // The little fire has been smothered by adding kindling too soon
                            setCampfireState(world, pos, EnumCampfireState.KINDLING);
                        }
                    }
                    else if (PrimalUtil.isLog(Block.getBlockFromItem(heldItem)))
                    {
                        // System.out.println("Holding log - smother");
                        tileEntity.setStokeTimer(0);
                        tileEntity.setTimesBlown(0);
                        setCampfireState(world, pos, EnumCampfireState.TINDER);
                    }
                    else if (heldItem == Items.WATER_BUCKET)
                    {
                        // System.out.println("Holding bucket");
                        putOutCampfire(world, pos, tileEntity);
                        return true;
                    }
                }
                else if (player.isSneaking()) // Holding nothing
                {
                    // System.out.println("Pick up tinder");
                    // Pick up the tinder
                    spawnAsEntity(world, pos, new ItemStack(ItemRegistry.TINDER));
                    world.setBlockToAir(pos);
                }
                else // Holding nothing
                {
                    attemptStokeCampfire(world, pos, tileEntity, (EntityPlayerMP) player, TINDER_MAX_BREATHS);
                }
                // System.out.println("(After) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
            }
            // KINDLING
            else if (campfireState == EnumCampfireState.KINDLING)
            {
                // System.out.println("KINDLING");
                // System.out.println("(Before) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
                if (!heldItemStack.isEmpty())
                {
                    // Shortcut to stoked kindling if you already have a burning torch
                    if ((DarkestConfig.basicItems.allowUnlitTorches && heldItem == Item.getItemFromBlock(BlockRegistry.TORCH_LIT)
                            || (DarkestConfig.basicItems.allowVanillaTorches && heldItem == Item.getItemFromBlock(Blocks.TORCH)))
                            && !world.isRainingAt(pos.up()))
                    {
                        // System.out.println("Holding torch");
                        tileEntity.setStokeTimer(STOKE_THRESHOLD);
                        tileEntity.setTimesBlown(KINDLING_MIN_BREATHS - 2); // Must blow at least twice before you can add logs
                        setCampfireState(world, pos, EnumCampfireState.KINDLING_STOKED);
                    }
                }
                else if (player.isSneaking()) // Holding nothing
                {
                    // System.out.println("Pick up kindling");
                    // Pick up the twigs and leave the tinder
                    spawnAsEntity(world, pos, new ItemStack(PrimalItemRegistry.TWIG_GENERIC, NUM_TWIGS_FOR_KINDLING));
                    setCampfireState(world, pos, EnumCampfireState.TINDER);
                }
                // System.out.println("(After) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
            }
            // STOKED KINDLING
            // Note: Stoked kindling always spawns small smoke particles in randomDisplayTick
            else if (campfireState == EnumCampfireState.KINDLING_STOKED)
            {
                // System.out.println("STOKED KINDLING");
                // System.out.println("(Before) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
                if (!heldItemStack.isEmpty())
                {
                    if (PrimalUtil.isLog(Block.getBlockFromItem(heldItem)))
                        // START BURNING - campfire state will be changed in TileEntityCampfire#onUpdate when it detects that there is fuel present
                        if (tileEntity.getTimesBlown() >= KINDLING_MIN_BREATHS)
                        {
                            // System.out.println("Holding log - start fire");
                            tileEntity.insertHeldItem(player, hand);
                            return true; // Don't open GUI or do anything else, since the desired interaction has been accomplished
                        }
                        else // Smother stoked kindling
                        {
                            // System.out.println("Holding log - smothered");
                            tileEntity.setStokeTimer(0);
                            tileEntity.setTimesBlown(0);
                            setCampfireState(world, pos, EnumCampfireState.KINDLING);
                        }
                    else if (heldItem == Items.WATER_BUCKET)
                    {
                        // System.out.println("Holding bucket");
                        putOutCampfire(world, pos, tileEntity);
                        return true;
                    }
                }
                else if (player.isSneaking()) // Holding nothing
                {
                    // System.out.println("Pick up kindling");
                    // Pick up the twigs and leave the tinder
                    spawnAsEntity(world, pos, new ItemStack(PrimalItemRegistry.TWIG_GENERIC, NUM_TWIGS_FOR_KINDLING));
                    setCampfireState(world, pos, EnumCampfireState.TINDER);
                }
                else // Holding nothing
                {
                    attemptStokeCampfire(world, pos, tileEntity, (EntityPlayerMP) player, KINDLING_MAX_BREATHS);
                }
                // System.out.println("(After) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
            }
            // BURNING
            else if (campfireState == EnumCampfireState.BURNING)
            {
                // System.out.println("BURNING");
                // System.out.println("(Before) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
                if (!heldItemStack.isEmpty())
                {
                    if (heldItem == Items.WATER_BUCKET)
                    {
                        // System.out.println("Holding bucket");
                        putOutCampfire(world, pos, tileEntity);
                        return true;
                    }
                    else
                    {
                        if (tileEntity.insertHeldItem(player, hand))
                        {
                            // System.out.println("Adding fuel to fire");
                            return true; // Don't open GUI or do anything else, since the desired interaction has been accomplished
                        }
                    }
                }
                // System.out.println("(After) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
            }
            // COALS
            // Note: Coals always spawn small smoke particles in randomDisplayTick
            else if (campfireState == EnumCampfireState.COALS)
            {
                // System.out.println("COALS");
                // System.out.println("(Before) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
                if (!heldItemStack.isEmpty())
                {
                    if (heldItem == Items.WATER_BUCKET)
                    {
                        // System.out.println("Holding bucket");
                        putOutCampfire(world, pos, tileEntity);
                        return true;
                    }
                    else
                    {
                        if (tileEntity.insertHeldItem(player, hand))
                        {
                            // System.out.println("Adding fuel to coals");
                            return true; // Don't open GUI or do anything else, since the desired interaction has been accomplished
                        }
                    }
                }
                // System.out.println("(After) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
            }
            // ASHES
            else if (campfireState == EnumCampfireState.ASHES)
            {
                // System.out.println("ASHES");
                // System.out.println("(Before) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
                if (!heldItemStack.isEmpty())
                {
                    if (heldItem instanceof ItemSpade && DarkestConfig.basicItems.allowCharcoalDust)
                    {
                        // System.out.println("Holding spade");
                        ItemStack stack = new ItemStack(ItemRegistry.DUST_CHARCOAL, DarkestConfig.itemScarcity.charcoalAmtAshes);

                        world.spawnEntity(new EntityItem(world, pos.getX(), pos.getY() + 0.5F, pos.getZ(), stack));
                        world.setBlockToAir(pos);
                    }
                    else if (heldItem == ItemRegistry.TINDER)
                    {
                        // System.out.println("Holding tinder");
                        setCampfireState(world, pos, EnumCampfireState.TINDER);
                        heldItemStack.shrink(1);
                    }
                }
                // System.out.println("(After) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
            }

            // OPEN GUI
            if (campfireState == EnumCampfireState.BURNING || campfireState == EnumCampfireState.COALS)
            {
                // System.out.println("Opening GUI");
                player.openGui(DarkestBeforeDawn.INSTANCE, GuiHandler.GUI_CAMPFIRE, world, pos.getX(), pos.getY(), pos.getZ());
            }

        }
        return true; // Don't place the held block when clicking this block
    }

    private void attemptStokeCampfire(World world, BlockPos pos, TileEntityCampfire tileEntity, EntityPlayerMP player, int burnoutThreshold)
    {
        // System.out.println("(Stoking) Stoke timer: " + tileEntity.getStokeTimer() + "; Times blown: " + tileEntity.getTimesBlown());
        // stokeTimer starts at MAX_STOKE_TIME and decrements each tick until it dips below STOKE_THRESHOLD
        // If MAX_STOKE_TIME is 140 and STOKE_THRESHOLD is 80, this means you can only blow once every 60 ticks, or 3 seconds
        if (tileEntity.getStokeTimer() < STOKE_THRESHOLD && !world.isRainingAt(pos.up()))
        {
            // System.out.println("Blowing on campfire");
            blowOnCampfire(world, pos, tileEntity, player);
        }

        if (tileEntity.getTimesBlown() >= burnoutThreshold) // Kindling is burned out - replace with ashes
        {
            // System.out.println("Putting out campfire");
            putOutCampfire(world, pos, tileEntity);
        }
    }

    private void blowOnCampfire(World world, BlockPos pos, TileEntityCampfire tileEntity, EntityPlayerMP player)
    {
        PacketHandler.INSTANCE.sendTo(new CampfirePacket(player, 0, pos), player); // blowing sound
        tileEntity.setStokeTimer(MAX_STOKE_TIME); // Blowing on the campfire refreshes the stoke timer
        tileEntity.setTimesBlown(tileEntity.getTimesBlown() + 1); // Blowing on campfire increases times blown - must blow a certain # of times to progress to next stage

        if (tileEntity.getTimesBlown() >= TINDER_MIN_BREATHS) // Tinder fully stoked - start spawning fire particles
        {
            // System.out.println("Tinder - Spawning flame");
            spawnFlame(world, pos);
        }

        if (tileEntity.getTimesBlown() >= KINDLING_MIN_BREATHS) // Kindling fully stoked - spawn extra flame and a big smoke particle
        {
            // System.out.println("Kindling - Spawning flame and big smoke");
            spawnFlame(world, pos);
            spawnBigSmoke(world, pos);
        }
    }

    private void putOutCampfire(World world, BlockPos pos, TileEntityCampfire tileEntity)
    {
        tileEntity.setStokeTimer(0);
        tileEntity.setTimesBlown(0);
        tileEntity.setBurnTimeInitial(0);
        tileEntity.setCoalsCoolDown(0);
        setCampfireState(world, pos, EnumCampfireState.ASHES);
        dropInventory(world, pos);
    }

    private void spawnFlame(World world, BlockPos pos)
    {
        double xPos = (double) pos.getX() + (Math.random() * 0.7D + 0.25D);
        double yPos = (double) pos.getY() + 0.1D;
        double zPos = (double) pos.getZ() + (Math.random() * 0.7D + 0.25D);

        PrimalUtilSpawn.spawnFlame(world, xPos, yPos, zPos);
    }

    private void spawnBigSmoke(World world, BlockPos pos)
    {
        double xPos = (double) pos.getX() + 0.5D;
        double yPos = (double) pos.getY() + 0.1D;
        double zPos = (double) pos.getZ() + 0.5D;

        PrimalUtilSpawn.spawnBigSmoke(world, xPos, yPos, zPos);
    }

    @Override
    public Class<TileEntityCampfire> getTileEntityClass()
    {
        return TileEntityCampfire.class;
    }

    @Nullable
    @Override
    public TileEntityCampfire createTileEntity(World world, IBlockState state)
    {
        return new TileEntityCampfire();
    }

    // Convert the given metadata into a BlockState for this Block
    @Nonnull
    public IBlockState getStateFromMeta(int meta)
    {

        switch (meta)
        {
            case 0:
                return this.getDefaultState().withProperty(CAMPFIRE_STATE, EnumCampfireState.TINDER);
            case 1:
                return this.getDefaultState().withProperty(CAMPFIRE_STATE, EnumCampfireState.TINDER_STOKED);
            case 2:
                return this.getDefaultState().withProperty(CAMPFIRE_STATE, EnumCampfireState.KINDLING);
            case 3:
                return this.getDefaultState().withProperty(CAMPFIRE_STATE, EnumCampfireState.KINDLING_STOKED);
            case 4:
                return this.getDefaultState().withProperty(CAMPFIRE_STATE, EnumCampfireState.BURNING);
            case 5:
                return this.getDefaultState().withProperty(CAMPFIRE_STATE, EnumCampfireState.COALS);
            case 6:
                return this.getDefaultState().withProperty(CAMPFIRE_STATE, EnumCampfireState.ASHES);
        }

        return this.getDefaultState();
    }

    // Convert the BlockState into the correct metadata value

    public int getMetaFromState(IBlockState state)
    {
        return state.getValue(CAMPFIRE_STATE).getMeta();
    }

    @Nonnull
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, CAMPFIRE_STATE);
    }

    /**
     * Called on both Client and Server when World#addBlockEvent is called. On the Server, this may perform additional
     * changes to the world, like pistons replacing the block with an extended base. On the client, the update may
     * involve replacing tile entities, playing assets.darkestbeforedawn.sounds, or performing other visual actions to reflect the server side
     * changes.
     */
    @SuppressWarnings("deprecation")
    public boolean eventReceived(IBlockState state, World worldIn, BlockPos pos, int id, int param)
    {
        super.eventReceived(state, worldIn, pos, id, param);
        TileEntity tileentity = worldIn.getTileEntity(pos);
        return tileentity != null && tileentity.receiveClientEvent(id, param);
    }

    public enum EnumCampfireState implements IStringSerializable
    {
        TINDER(0, "tinder"),
        TINDER_STOKED(1, "tinder_stoked"),
        KINDLING(2, "kindling"),
        KINDLING_STOKED(3, "kindling_stoked"),
        BURNING(4, "burning"),
        COALS(5, "coals"),
        ASHES(6, "ashes");

        private final int meta;
        private final String name;

        EnumCampfireState(int meta, String name)
        {
            this.meta = meta;
            this.name = name;
        }

        @Override
        @Nonnull
        public String getName()
        {
            return name;
        }

        int getMeta()
        {
            return meta;
        }
    }

}