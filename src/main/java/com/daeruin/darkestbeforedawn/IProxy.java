package com.daeruin.darkestbeforedawn;

interface IProxy
{
    @SuppressWarnings("EmptyMethod")
    void preInit();

    void init();

    void postInit();
}
