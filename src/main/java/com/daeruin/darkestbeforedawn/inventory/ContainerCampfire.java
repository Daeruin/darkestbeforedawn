package com.daeruin.darkestbeforedawn.inventory;

import com.daeruin.darkestbeforedawn.blocks.BlockTileEntityCampfire;
import com.daeruin.darkestbeforedawn.recipes.CampfireRecipes;
import com.daeruin.darkestbeforedawn.tileentity.TileEntityCampfire;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

public class ContainerCampfire extends Container
{

    private static final int
            SLOT_INPUT = 0,
            SLOT_FUEL = 1,
            SLOT_OUTPUT = 2,
            INV_START = SLOT_OUTPUT + 1,    // 3
            INV_END = INV_START + 26,        // 29
            HOT_BAR_START = INV_END + 1,    // 30
            HOT_BAR_END = HOT_BAR_START + 8;    // 38
    private final TileEntityCampfire tileEntityCampfire;
    private int[] cachedFields;

    public ContainerCampfire(InventoryPlayer playerInventory, TileEntityCampfire tileEntityCampfire)
    {
        this.tileEntityCampfire = tileEntityCampfire;
        IItemHandler inventory = tileEntityCampfire.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

        // Input slot
        this.addSlotToContainer(new SlotItemHandler(inventory, 0, 56, 17)
        {
            // This enforces the slot limit for mergeItemStack and transferStackInSlot
            @Override
            public int getSlotStackLimit()
            {
                return 1;
            }
        });

        // Fuel slot
        this.addSlotToContainer(new SlotCampfireFuel(inventory, 1, 56, 53));

        // Output slot
        this.addSlotToContainer(new SlotCampfireOutput(playerInventory.player, inventory, 2, 116, 35));

        // Player inventory
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        // Hot bar
        for (int k = 0; k < 9; ++k)
        {
            this.addSlotToContainer(new Slot(playerInventory, k, 8 + k * 18, 142));
        }
    }

    public void addListener(IContainerListener listener)
    {
        super.addListener(listener);
        listener.sendWindowProperty(this, TileEntityCampfire.FIELD_BURN_TIME_INITIAL, (this.tileEntityCampfire).getBurnTimeInitial());
        listener.sendWindowProperty(this, TileEntityCampfire.FIELD_BURN_TIME_REMAINING, (this.tileEntityCampfire).getBurnTimeRemaining());
        listener.sendWindowProperty(this, TileEntityCampfire.FIELD_COOK_TIME, (this.tileEntityCampfire).getCookTimer());
        listener.sendWindowProperty(this, TileEntityCampfire.FIELD_TOTAL_COOK_TIME, (this.tileEntityCampfire).getTotalCookTime());
    }

    /**
     * Looks for changes made in the container, sends them to every listener.
     */
    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();

        boolean allFieldsHaveChanged = false;
        boolean fieldHasChanged[] = new boolean[tileEntityCampfire.getFieldCount()];
        if (cachedFields == null)
        {
            cachedFields = new int[tileEntityCampfire.getFieldCount()];
            allFieldsHaveChanged = true;
        }
        for (int i = 0; i < cachedFields.length; ++i)
        {
            if (allFieldsHaveChanged || cachedFields[i] != tileEntityCampfire.getField(i))
            {
                cachedFields[i] = tileEntityCampfire.getField(i);
                fieldHasChanged[i] = true;
            }
        }
        for (IContainerListener listener : this.listeners)
        {
            for (int fieldID = 0; fieldID < tileEntityCampfire.getFieldCount(); ++fieldID)
            {
                if (fieldHasChanged[fieldID])
                {
                    listener.sendWindowProperty(this, fieldID, cachedFields[fieldID]);
                }
            }
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int data)
    {
        this.tileEntityCampfire.setField(id, data);
    }

    /**
     * Determines whether supplied player can use this container
     */
    @Override
    @ParametersAreNonnullByDefault
    public boolean canInteractWith(EntityPlayer playerIn)
    {
        BlockTileEntityCampfire.EnumCampfireState campfireState = tileEntityCampfire.getWorld().getBlockState(tileEntityCampfire.getPos()).getValue(BlockTileEntityCampfire.CAMPFIRE_STATE);
        return campfireState == BlockTileEntityCampfire.EnumCampfireState.COALS || campfireState == BlockTileEntityCampfire.EnumCampfireState.BURNING;
    }

    /**
     * Take a stack from the specified inventory slot and transfer it to another appropriate slot.
     * Handles when a slot is shift-clicked. Should automatically send clicked item to appropriate slot.
     */
    @Nonnull
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
        ItemStack copyOfItemStack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack originalItemStack = slot.getStack();
            copyOfItemStack = originalItemStack.copy();

            // Send from output to main inventory or hot bar
            if (index == SLOT_OUTPUT)
            {
                if (!this.mergeItemStack(originalItemStack, INV_START, HOT_BAR_END + 1, true))
                {
                    return ItemStack.EMPTY;
                }

                slot.onSlotChange(originalItemStack, copyOfItemStack); // Generate XP orbs and achievements
            }
            // Send from main inventory or hot bar...
            else if (index != SLOT_FUEL && index != SLOT_INPUT)
            {
                // ...to input slot, if item has a smelting result
                if (!CampfireRecipes.instance().getCookingResult(originalItemStack).isEmpty())
                {
                    if (!this.mergeItemStack(originalItemStack, SLOT_INPUT, SLOT_INPUT + 1, false))
                    {
                        return ItemStack.EMPTY;
                    }
                }
                // ...to fuel slot, if item is fuel
                else if (TileEntityCampfire.isItemFuel(originalItemStack))
                {
                    if (!this.mergeItemStack(originalItemStack, SLOT_FUEL, SLOT_FUEL + 1, false))
                    {
                        return ItemStack.EMPTY;
                    }
                }
                // ...to hot bar, if item is in main inventory
                else //noinspection ConstantConditions
                    if (index >= INV_START && index <= INV_END)
                    {
                        if (!this.mergeItemStack(originalItemStack, HOT_BAR_START, HOT_BAR_END + 1, false))
                        {
                            return ItemStack.EMPTY;
                        }
                    }
                    // ...to main inventory, if item is in hot bar
                    else //noinspection ConstantConditions
                        if (index >= HOT_BAR_START && index <= HOT_BAR_END && !this.mergeItemStack(originalItemStack, INV_START, INV_END + 1, false))
                        {
                            return ItemStack.EMPTY;
                        }
            }
            // Send from anywhere to main inventory or hot bar
            else if (!this.mergeItemStack(originalItemStack, INV_START, HOT_BAR_END + 1, false))
            {
                return ItemStack.EMPTY;
            }

            if (originalItemStack.getCount() == 0)
            {
                slot.putStack(ItemStack.EMPTY);
            }
            else
            {
                slot.onSlotChanged();
            }

            // Nothing was moved
            if (originalItemStack.getCount() == copyOfItemStack.getCount())
            {
                return ItemStack.EMPTY;
            }

            slot.onTake(playerIn, originalItemStack);
        }

        return copyOfItemStack;
    }

    // This override (courtesy coolAlias, modified by me) respects the item limit for the slot by using slot.getSlotStackLimit
    @Override
    protected boolean mergeItemStack(ItemStack incomingStack, int start, int end, boolean backwards)
    {
        boolean flag1 = false;
        int k = (backwards ? end - 1 : start);
        Slot slot;
        ItemStack destinationStack;

        // As long as incoming item is stackable,
        // look for other slots containing the same type of item and fill them up first
        if (incomingStack.isStackable())
        {
            while (incomingStack.getCount() > 0 && (!backwards && k < end || backwards && k >= start))
            {
                slot = inventorySlots.get(k);
                destinationStack = slot.getStack();

                // If item isn't valid for the slot,
                // or the destination slot is empty (this is handled later in this method),
                // or the destination slot is already full,
                // or the destination stack is already at its limit,
                // move on to the next slot
                if (!slot.isItemValid(incomingStack) || destinationStack.isEmpty() || destinationStack.getCount() >= slot.getSlotStackLimit() || destinationStack.getCount() >= incomingStack.getMaxStackSize())
                {
                    k += (backwards ? -1 : 1);
                    continue;
                }

                // If incoming item and destination item are identical
                if (!destinationStack.isEmpty() && destinationStack.getItem() == incomingStack.getItem() &&
                        (!incomingStack.getHasSubtypes() || incomingStack.getItemDamage() == destinationStack.getItemDamage()) &&
                        ItemStack.areItemStackTagsEqual(incomingStack, destinationStack))
                {
                    int combinedSize = destinationStack.getCount() + incomingStack.getCount();

                    // If combined size fits in stack limit and slot limit,
                    // set destination to combined size and reduce incoming to 0
                    if (combinedSize <= incomingStack.getMaxStackSize() && combinedSize <= slot.getSlotStackLimit())
                    {
                        incomingStack.setCount(0);
                        destinationStack.setCount(combinedSize);
                        this.tileEntityCampfire.markDirty();
                        flag1 = true;
                    }
                    // At this point, we know there is room in the slot and the stack, but not enough for everything.
                    // Figure out how much space is left - the lesser of the stack limit or the slot limit.
                    // Pull that much out of the incoming stack and add it to the destination stack.
                    else //if (destinationStack.stackSize < incomingStack.getMaxStackSize() && combinedSize < slot.getSlotStackLimit())
                    {
                        int spaceLeft = Math.min(incomingStack.getMaxStackSize(), slot.getSlotStackLimit()) - destinationStack.getCount();
                        incomingStack.shrink(spaceLeft);
                        destinationStack.grow(spaceLeft);
                        this.tileEntityCampfire.markDirty();
                        flag1 = true;
                    }
                }

                k += (backwards ? -1 : 1);
            }
        }

        // If incoming item isn't stackable, or incoming stack still has items left in it,
        // look for empty slots to put the item(s) in
        if (incomingStack.getCount() > 0)
        {
            k = (backwards ? end - 1 : start);

            while (!backwards && k < end || backwards && k >= start)
            {
                slot = inventorySlots.get(k);
                destinationStack = slot.getStack();

                if (!slot.isItemValid(incomingStack))
                {
                    k += (backwards ? -1 : 1);
                    continue;
                }

                // Since item isn't stackable (or all slots with this item are already full), we only care about empty slots now
                if (destinationStack.isEmpty())
                {
                    // If incoming stack fits in slot limit, put the whole stack in the slot
                    if (incomingStack.getCount() <= slot.getSlotStackLimit())
                    {
                        slot.putStack(incomingStack.copy());
                        incomingStack.setCount(0);
                        this.tileEntityCampfire.markDirty();
                        flag1 = true;
                        break;
                    }
                    else // Put as much in the slot as will fit and reduce incoming stack by corresponding amount
                    {
                        putStackInSlot(k, new ItemStack(incomingStack.getItem(), slot.getSlotStackLimit(), incomingStack.getItemDamage()));
                        incomingStack.shrink(slot.getSlotStackLimit());

                        this.tileEntityCampfire.markDirty();
                        flag1 = true;
                    }
                }

                k += (backwards ? -1 : 1);
            }
        }

        return flag1;
    }

}
