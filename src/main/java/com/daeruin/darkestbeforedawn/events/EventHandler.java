package com.daeruin.darkestbeforedawn.events;

import com.daeruin.darkestbeforedawn.blocks.BlockRegistry;
import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.darkestbeforedawn.items.ItemBlockTorchLit;
import com.daeruin.darkestbeforedawn.items.ItemRegistry;
import com.daeruin.darkestbeforedawn.utilities.DarkestUtil;
import com.daeruin.primallib.blocks.PrimalBlockLog;
import com.daeruin.primallib.blocks.PrimalBlockRegistry;
import com.daeruin.primallib.util.PrimalUtil;
import com.daeruin.primallib.util.PrimalUtilSpawn;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.BlockOldLog;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import net.minecraft.world.WorldProviderSurface;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Iterator;
import java.util.Random;

import static net.minecraft.block.BlockPlanks.EnumType.SPRUCE;

public class EventHandler
{
    private int updateLCG = (new Random()).nextInt(); // Used to spawn resin

    // MAKE IT DARK
    // Based on weather and distance underground
    // Takes direct control of the brightness slider. If you change the slider manually, this event will immediately change it back.

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onClientTick(TickEvent.ClientTickEvent event)
    {
        // If the darkness feature is disabled in the config, don't do anything to change the gamma setting here
        if (!DarkestConfig.darknessBehavior.enableDarkness)
        {
            return;
        }

        Minecraft minecraft = Minecraft.getMinecraft();
        EntityPlayerSP player = minecraft.player;
        World world = minecraft.world;

        // Only change stuff if player is in the world
        if (player == null || world == null)
        {
            return;
        }

        // End phase and player is in Overworld
        if (event.phase == TickEvent.Phase.END && player.dimension == 0)
        {
            if (player.ticksExisted % DarkestConfig.darknessBehavior.lightCheckFrequency == 0)
            {
                // Pos of player's head
                BlockPos playerPos = new BlockPos(player.posX, player.posY + player.getEyeHeight(), player.posZ);
                // Light value of 0 to 15 at player's head - from light-giving blocks like torches, glow stone, etc.
                int crudeBlockLight = world.getLightFor(EnumSkyBlock.BLOCK, playerPos);
                // Light value of 0 to 15 at player's head - value is 15 if sky is directly overhead, even at night, and goes down as you move further into a tunnel or overhang
                int crudeSkyLight = world.getLightFor(EnumSkyBlock.SKY, playerPos);
                // These are the values we need to find out - assume no light to start out
                float preciseBlockLight = 0F;
                float outdoorLight = 0F;
                float preciseSkyLight = 0F;

                // If crude block light is over 0, it means the player is close enough to a light block to be getting some light from it, so get the exact amount of light from the block
                if (crudeBlockLight > 0)
                    preciseBlockLight = getPreciseLightFromSource(world, player, EnumSkyBlock.BLOCK);
                // If crude sky light is over 0, it means the player is close enough to a sky block for outdoor light to potentially matter, so get the amount of outdoor light (from sun, moon, clouds)
                if (crudeSkyLight > 0)
                    outdoorLight = getOutdoorLight(world);
                // If crude sky light is 15, the player is exposed to the sky and there is no need to calculate their exact distance from the sky light
                if (crudeSkyLight >= 15)
                    preciseSkyLight = 1.0F;
                    // If outdoor light is over 0 (but < 15), it means the player is getting some light from the sky, so get the exact amount of light from the sky source
                else if (outdoorLight > 0)
                    preciseSkyLight = getPreciseLightFromSource(world, player, EnumSkyBlock.SKY);
                // If outdoor light is 0, that means it's a new moon or it's a small moon with cloud cover - in that case, there will be no light from the sky and we can use the default of 0.0F

                // Precise sky light and outdoor light are both values 0 to 1.0
                // Precise sky light basically tells you how far underground you are
                // Multiplying outdoor light by precise sky light scales the outdoor light down the further underground you go
                float light = Math.max(preciseBlockLight, preciseSkyLight * outdoorLight);
                // Scale and shift the light values
                // Darkness level from config is 0 to 1.0F - this sets the lowest level of darkness possible - ceiling is always 1.0F
                // At darknessLevel of 1.0, 0 to 1.0 becomes -1.0 to 1.0
                // At darknessLevel of 0.5, 0 to 1.0 becomes -0.5 to 1.0
                float darknessLevel = DarkestConfig.darknessBehavior.darknessLevel;
                // light = (light * 2F) - 1F; // This was the original formula
                light = (light * (darknessLevel + 1F)) - darknessLevel;
                // Set player gamma setting to the calculated light value
                minecraft.gameSettings.gammaSetting = light;

                if (player.ticksExisted % 40 == 0)
                {
                    // System.out.println("Gamma: " + minecraft.gameSettings.gammaSetting + "; Pos: " + player.posX + ", " + player.posZ + "; Axis: " + lightAxis + "; Dir: " + lightDirection + "; Dist: " + distanceInBlock);
                    // System.out.println("Player light: " + playerLight + "; Final light: " + undergroundLight + "; 1/15: " + 1/15F + "; %: " + ((1/15F)*distanceInBlock) + "; Diff: " + (0.1F-((1/15F)*distanceInBlock)) + "; Axis: " + lightAxis + "; Pos in light dir: " + playerPosInLightDirection + "; Pos in block: " + distanceInBlock + "; Block light: " + world.getLightFromNeighbors(playerPos) + "; Light table: " + Arrays.toString(world.provider.getLightBrightnessTable()));
                    // System.out.println("Gamma: " + minecraft.gameSettings.gammaSetting + "; WorldTime: " + worldTime + "; TotalTime: " + totalWorldTime + "; Sub: " + world.getSkylightSubtracted() + "; Calc: " + world.calculateSkylightSubtracted(worldTime) + "; Sun: " + world.getSunBrightness(worldTime) + "; Factor: " + world.getSunBrightnessFactor(worldTime) + "; Body: " + world.getSunBrightnessBody(worldTime) + "; MoonPhase: " + world.getMoonPhase() + "; MoonFactor: " + world.getCurrentMoonPhaseFactor() + "; MoonBody: " + world.getCurrentMoonPhaseFactorBody());
                    // System.out.println("WorldTime: " + worldTime + "; AdjTime: " + adjustedTime + "; " + time + "; Daylight factor: " + sunLight);
                    // System.out.println("Gamma: " + minecraft.gameSettings.gammaSetting + "; MoonPhase: " + world.getMoonPhase() + "; MoonFactor: " + world.getCurrentMoonPhaseFactor() + "; MoonLightFactor: " + moonLight + "; RainFactor: " + rainFactor + "; ThunderFactor: " + thunderFactor);
                    // System.out.println("Player brightness: " + player.getBrightness() + "; For render: " + player.getBrightnessForRender());
                    // System.out.println("Gamma: " + minecraft.gameSettings.gammaSetting + "; Bright: " + brightestLight + "; Neigh: " + world.getLightFromNeighbors(playerPos) + "; Sky light: " + world.getLightFor(EnumSkyBlock.SKY, playerPos) + "; Block light: " + world.getLightFor(EnumSkyBlock.BLOCK, playerPos) + "; Dir: " + lightDirection + "; Axis: " + lightAxis + "; Pos: " + playerPosInLightDirection + "; Light: " + lightInDirection + "; UG: " + undergroundLight);
                    // System.out.println("Gamma: " + minecraft.gameSettings.gammaSetting + "; Crude: " + crudeSkyLight + "; Outdoor: " + outdoorLight + "; Precise: " + preciseSkyLight);
                    // System.out.println("Gamma: " + minecraft.gameSettings.gammaSetting + "; Crude block: " + crudeBlockLight + "; Precise block: " + preciseBlockLight + "; Crude sky: " + crudeSkyLight + "; Outdoor: " + outdoorLight + "; Precise sky: " + preciseSkyLight + "; See sky? " + world.canSeeSky(playerPos) + "; Valid? " + world.isValid(playerPos) + "; Loaded? " + world.isBlockLoaded(playerPos));
                }
            }
        }
    }

    /*
        Returns a value from 0 to 1.0F
        Represents how far away the player is from the edge of the block giving the brightest light
        0.01F would be right next to the edge of the light block
        0.99F would be as far away from the edge of the light block you could get before you cross over to the next block
        1.0F would mean the player is already in the block with the brightest light
    */
    private float getPreciseLightFromSource(World world, EntityPlayerSP player, EnumSkyBlock lightType)
    {
        float preciseLight;
        BlockPos playerPos = new BlockPos(player.posX, player.posY + player.getEyeHeight(), player.posZ); // Pos of player's head
        int playerPosLight = world.getLightFor(lightType, playerPos); // Light at the player's position
        int brightestLight = 0; // Should be in range 0 to 15
        EnumFacing brightestLightFacing = EnumFacing.UP; // Direction of brightest light compared to player (up, down, east, west, north, or south)

        // Find brightest block that's neighboring the player's head
        for (EnumFacing facingToCheck : EnumFacing.values())
        {
            int lightAtFacing = world.getLightFor(lightType, playerPos.offset(facingToCheck));

            if (lightAtFacing > brightestLight)
            {
                brightestLight = lightAtFacing;
                brightestLightFacing = facingToCheck;
            }
        }

        // If the brightest neighboring block is still not brighter than the player's position
        // Then the player is already in the brightest light and we don't need to calculate their exact distance from the light
        // They are already at full brightness (1.0)
        if (brightestLight <= playerPosLight)
        {
            preciseLight = 1.0F;
        }
        // Otherwise, the brightest light is in a neighboring block, and we need to calculate the player's exact distance from that light
        else
        {
            // Get the axis the brightest light is on compared with the player - e.g. if light is north or south of player, axis is Z
            EnumFacing.Axis lightAxis = brightestLightFacing.getAxis();
            // The block pos of the brightest light
            BlockPos lightPos = playerPos.offset(brightestLightFacing);
            double playerPosInLightDirection = 0;
            int brightestLightPosOnAxis = 0;

            // Check axis of brightest light, then get:
            // * the player's exact pos along that axis
            // * the general pos of the brightest light along that axis
            // The comparison of these two values determines which formula we use to calculate the player's exact distance from the light
            if (lightAxis == EnumFacing.Axis.X)
            {
                playerPosInLightDirection = player.posX;
                brightestLightPosOnAxis = lightPos.getX();
            }
            else if (lightAxis == EnumFacing.Axis.Y)
            {
                playerPosInLightDirection = player.posY + player.getEyeHeight();
                brightestLightPosOnAxis = lightPos.getY();
            }
            else if (lightAxis == EnumFacing.Axis.Z)
            {
                playerPosInLightDirection = player.posZ;
                brightestLightPosOnAxis = lightPos.getZ();
            }

            // Chop off the decimals and subtract that from the number with decimals - this leaves only the decimals - number from 0 to 1.0
            // Represents how far away the player is from the edge of the block giving the brightest light
            float distanceInBlock = (float) playerPosInLightDirection - (int) playerPosInLightDirection;
            // Get the absolute value
            distanceInBlock = Math.abs(distanceInBlock);

            // Light fine-tuned based on exact distance away from brightest light
            // This formula was really hard to figure out, and I have a diagram drawn on a piece of paper to illustrate it
            preciseLight = Math.abs(brightestLightPosOnAxis) < Math.abs(playerPosInLightDirection) ? brightestLight - distanceInBlock : brightestLight + distanceInBlock - 1;
            // Precise light should not be higher than 15, but if it were then we should use 15 instead - then divide by 15 to get a value between 0 and 1
            preciseLight = Math.min(15, preciseLight) / 15;
        }

        return preciseLight; // Either 1.0 if player is already in the brightest light, or something from 0 to 0.99 if player is next to the brightest light
    }

    private float getOutdoorLight(World world)
    {
        // TIME OF DAY

        // worldTime is starts at 0  (morning) on the first day of the world's existence, then increments every tick
        // worldTime matches totalWorldTime unless the user manually sets the world time with commands
        // Shift worldTime forward so 6am (0 ticks) is like 12pm (6000 ticks)
        // This turns Minecraft time into a 24 hour clock - just divide by 1000 to get 24-hour clock time, i.e. 8000 = 8am
        float adjustedTime = (world.getWorldTime() + 6000) % 24000;
        int SUNRISE_START = 3800; // In adjustedTime
        int SUNSET_START = 17800; // In adjustedTime
        int TRANSITION_LENGTH = 2200; // In adjustedTime
        float sunLight;

        // Midnight to sunrise = 0-3800
        if (adjustedTime < SUNRISE_START)
        {
            sunLight = 0.0F;
        }
        // Sunrise = 3800-6000
        else if (adjustedTime < SUNRISE_START + TRANSITION_LENGTH)
        {
            sunLight = (adjustedTime - SUNRISE_START) / TRANSITION_LENGTH;
        }
        // End of sunrise to sunset = 6000-17800
        else if (adjustedTime < SUNSET_START)
        {
            sunLight = 1.0F;
        }
        // Sunset = 17800-20000
        else if (adjustedTime < SUNSET_START + TRANSITION_LENGTH)
        {
            sunLight = 1.0F - (adjustedTime - SUNSET_START) / TRANSITION_LENGTH;
        }
        // Sunset to midnight = 20000-24000
        else
        {
            sunLight = 0.0F;
        }

        // MOONLIGHT

        // moonPhaseFactor goes down as moon goes toward a new moon - float between 1.0 and 0.0, in steps of .25
        // Phase change completes every 24,000 ticks, i.e. once per day, right after it sets
        // So it goes from full to new in 4 days, then back to full in 4 more days
        float moonPhase = world.getCurrentMoonPhaseFactor();
        // When moonPhaseFactor is 1.0 (full), light will be 0.5F; when it's 0.0 (new moon), light will be 0.0F
        float moonLight = moonPhase / 2;
        // We want the brightest source of outdoor light - if the sun is down, the moon will be the source of light
        float outdoorLight = Math.max(sunLight, moonLight);

        // WEATHER

        // getRainStrength and getThunderStrength go from 0.0 to 1.0 over a few seconds as it begins raining or thundering
        // If they start at the same time, they both go up at the same time. If thundering starts after rain, it goes up independently.
        // isRaining becomes true when rainStrength > 0.2
        // isThundering becomes true when thunderStrength > 0.9
        float rainFactor = world.getRainStrength(1.0F) + 1;
        float thunderFactor = world.getThunderStrength(1.0F) + 1;
        // When rain and thunder are 0, outdoor light is unchanged. When they are both 1.0, outdoor light is halved, then halved again.
        // This makes it pretty darn dark when it's thundering during the day, maybe too dark - consider adjusting this
        outdoorLight = outdoorLight / rainFactor / thunderFactor;

        return outdoorLight;
    }

    // DON'T LET VANILLA TORCHES JOIN THE WORLD AS ENTITY ITEMS

    @SubscribeEvent
    public void onEntitySpawn(EntityJoinWorldEvent event)
    {
        if (!DarkestConfig.basicItems.allowVanillaTorches)
        {
            Entity entity = event.getEntity();

            if (entity instanceof EntityItem)
            {
                if (((EntityItem) entity).getItem().getItem() == Item.getItemFromBlock(Blocks.TORCH))
                {
                    event.setCanceled(true);
                }
            }
        }
    }

    // SPAWN ANIMAL FAT

    @SubscribeEvent
    public void onLivingDrops(LivingDropsEvent event)
    {
        if (DarkestConfig.basicItems.allowAnimalFat)
        {
            Entity entity = event.getEntity();
            int amount = 0;

            for (String animalFatSource : DarkestConfig.itemScarcity.animalFatSources)
            {
                try
                {
                    String[] fatSourceParsed = animalFatSource.split(",");

                    if (fatSourceParsed.length == 0)
                    {
                        System.err.println("Animal fat source " + animalFatSource + " is not structured correctly. Please fix your config file.");
                        continue;
                    }

                    ResourceLocation resourceLocation = new ResourceLocation(fatSourceParsed[0]);

                    if (EntityList.isMatchingName(entity, resourceLocation))
                    {
                        amount = Integer.parseInt(fatSourceParsed[1]);
                    }
                }
                catch (Exception exception)
                {
                    System.err.println("Failed to parse " + animalFatSource + ". " + exception);
                }
            }

            if (amount > 0)
            {
                World world = entity.getEntityWorld();
                BlockPos pos = entity.getPosition();
                EntityItem animalFat = new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(ItemRegistry.ANIMAL_FAT_RAW, amount));

                event.getDrops().add(animalFat);
            }
        }
    }

    // SPAWN RESIN

    @SubscribeEvent
    public void onWorldTick(TickEvent.WorldTickEvent event) // It's always on Side.SERVER
    {
        if (event.phase == TickEvent.Phase.END)
        {
            WorldServer world = (WorldServer) event.world;

            if (world.provider instanceof WorldProviderSurface && world.rand.nextInt(100) == 0) // Only for Overworld, not Nether or End
            {
                Iterator<Chunk> chunkIterator = world.getPlayerChunkMap().getChunkIterator();

                while (chunkIterator.hasNext())
                {
                    Chunk chunk = chunkIterator.next();
                    int chunkCornerX = chunk.x * 16;
                    int chunkCornerZ = chunk.z * 16;
                    this.updateLCG = this.updateLCG * 3 + 1013904223;
                    int rand = this.updateLCG >> 2;
                    int columnX = chunkCornerX + (rand & 15);
                    int columnZ = chunkCornerZ + (rand >> 8 & 15);
                    BlockPos randomPosTop = world.getPrecipitationHeight(new BlockPos(columnX, 0, columnZ));
                    int resinCheckingStage = 0;
                    BlockPos resinSource = null;

                    for (int y = randomPosTop.getY(); y > 0; y--) // Work from precipitation height down to bottom of world
                    {
                        BlockPos posToCheck = new BlockPos(columnX, y, columnZ);
                        IBlockState stateToCheck = world.getBlockState(posToCheck);
                        Block blockToCheck = stateToCheck.getBlock();

                        switch (resinCheckingStage)
                        {
                            case 0: // Looking for spruce leaves
                                if (DarkestUtil.isSpruceLeaf(stateToCheck))
                                    resinCheckingStage = 1;
                                break;
                            case 1: // Found spruce leaves, now looking for spruce logs
                                if (DarkestUtil.isSpruceLeaf(stateToCheck))
                                    continue;
                                else if (DarkestUtil.isSpruceLog(stateToCheck))
                                    resinCheckingStage = 2;
                                else
                                    resinCheckingStage = 0;
                                break;
                            case 2: // Found spruce logs, now looking for stripped spruce log
                                if (DarkestUtil.isSpruceLog(stateToCheck))
                                    continue;
                                else if (blockToCheck == PrimalBlockRegistry.STRIPPED_SPRUCE_LOG)
                                {
                                    resinSource = posToCheck;
                                    resinCheckingStage = 3;
                                }
                                else
                                    resinCheckingStage = 0;
                                break;
                            case 3: // Found stripped spruce, now looking for spruce log
                                if (blockToCheck == PrimalBlockRegistry.STRIPPED_SPRUCE_LOG)
                                {
                                    resinSource = posToCheck;
                                    continue;
                                }
                                else if (DarkestUtil.isSpruceLog(stateToCheck))
                                {
                                    resinCheckingStage = 4;
                                }
                                else
                                    resinCheckingStage = 0;
                                break;
                            case 4: // Found spruce log, now looking for dirt
                                if (DarkestUtil.isSpruceLog(stateToCheck))
                                    continue;
                                else if (blockToCheck == PrimalBlockRegistry.STRIPPED_SPRUCE_LOG)
                                {
                                    resinCheckingStage = 3;
                                    resinSource = posToCheck;
                                    continue;
                                }
                                else if (blockToCheck instanceof BlockDirt)
                                {
                                    world.setBlockState(resinSource.down(), BlockRegistry.LOG_SPRUCE_RESINOUS.getDefaultState().withProperty(PrimalBlockLog.AXIS, EnumFacing.Axis.Y));
                                    resinCheckingStage = 0;
                                }
                                else
                                    resinCheckingStage = 0;
                                break;
                            default:
                                resinCheckingStage = 0;
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void onHarvestDrops(BlockEvent.HarvestDropsEvent event)
    {
        if (event.getHarvester() == null)
        {
            return;
        }

        EntityPlayer player = event.getHarvester();

        if (!DarkestConfig.itemScarcity.requireSneaking || player.isSneaking())
        {
            IBlockState state = event.getState();
            float chance = DarkestConfig.itemScarcity.spruceConeDropChance;

            // SPAWN SPRUCE CONES

            if (DarkestConfig.basicItems.allowSpruceConeTorches)
            {
                if (DarkestUtil.isSpruceLeaf(state))
                {
                    if (chance > 0.0F)
                    {
                        PrimalUtil.addDropWithChance(event, ItemRegistry.SPRUCE_CONE, chance, DarkestConfig.itemScarcity.spruceConeDropRate);
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void onRightClickBlock(PlayerInteractEvent.RightClickBlock event)
    {
        if (!event.getWorld().isRemote && event.getResult() != Event.Result.DENY)
        {
            World world = event.getWorld();
            BlockPos pos = event.getPos();
            IBlockState state = world.getBlockState(pos);
            Block block = state.getBlock();
            Material material = state.getMaterial();

            // Items should be spawned next to the face that was clicked
            BlockPos destination = pos;
            EnumFacing faceClicked = event.getFace();

            if (faceClicked != null)
            {
                destination = destination.offset(faceClicked);
            }

            // Placeholders for item to be gathered
            Item itemToBeGathered = null;
            int amount = 0;

            // RESINOUS SPRUCE LOG -> RESIN

            if (DarkestConfig.basicItems.allowResin && block == BlockRegistry.LOG_SPRUCE_RESINOUS)
            {
                // Replace resinous log with vanilla log
                world.setBlockState(event.getPos(), Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, SPRUCE));
                itemToBeGathered = ItemRegistry.RESIN;
                amount = DarkestConfig.itemScarcity.resinDropRate;
                // Spawn resin
                // PrimalUtil.spawnEntityItem(world, destination, ItemRegistry.RESIN, DarkestConfig.general.resinDropRate);
            }

            if (material == Material.ROCK || material == Material.ANVIL || material == Material.WOOD || material == Material.IRON)
            {
                ItemStack heldItem = event.getItemStack();

                // CHARCOAL -> CHARCOAL DUST

                if (DarkestConfig.basicItems.allowCharcoalDust && heldItem.getItem() == Items.COAL && heldItem.getMetadata() == 1) // 1 is charcoal
                {
                    heldItem.shrink(1);
                    itemToBeGathered = ItemRegistry.DUST_CHARCOAL;
                    amount = DarkestConfig.itemScarcity.charcoalAmtCharcoal;
                    // PrimalUtil.spawnEntityItem(world, destination, ItemRegistry.DUST_CHARCOAL, 1);
                }

                // SUGAR CANE -> FRAYED CANE

                else if (DarkestConfig.basicItems.allowCaneTorches && heldItem.getItem() == Items.REEDS)
                {
                    heldItem.shrink(1);
                    itemToBeGathered = ItemRegistry.FRAYED_CANE;
                    amount = 1;
                }
            }

            if (itemToBeGathered != null)
            {
                boolean addedStackToInventory = event.getEntityPlayer().addItemStackToInventory(new ItemStack(itemToBeGathered, amount));

                if (!addedStackToInventory)
                {
                    PrimalUtilSpawn.spawnEntityItem(world, destination, itemToBeGathered, amount);
                }
            }
        }
    }

    // LIT TORCH PICKED UP WITH EMPTY HAND ONLY

    // Called from EntityItem#onCollideWithPlayer
    // Canceling the event returns the method immediately, preventing the item from being picked up
    @SubscribeEvent
    public void onEntityItemPickup(EntityItemPickupEvent event)
    {
        EntityItem entityItem = event.getItem();
        ItemStack itemStack = entityItem.getItem(); // Copy of original ItemStack from when EntityItem was created - with old damage value
        Item item = itemStack.getItem();

        if (DarkestConfig.torchBehavior.torchCanOnlyBeHeldInHand)
        {
            EntityPlayer player = event.getEntityPlayer();

            if (itemStack.getItem() instanceof ItemBlockTorchLit)
            {
                if (DarkestConfig.torchBehavior.preferOffHand && player.getHeldItemOffhand().isEmpty())
                {
                    pickUpTorch(entityItem, itemStack, player, EnumHand.OFF_HAND);
                }
                else if (player.getHeldItemMainhand().isEmpty())
                {
                    pickUpTorch(entityItem, itemStack, player, EnumHand.MAIN_HAND);
                }
                else if (player.getHeldItemOffhand().isEmpty())
                {
                    pickUpTorch(entityItem, itemStack, player, EnumHand.OFF_HAND);
                }

                event.setCanceled(true); // If neither hand is empty, don't pick up item
            }
        }

        if (item == ItemRegistry.GLOWING_EMBER
                && !event.getEntityPlayer().getHeldItemMainhand().isEmpty())
        {
            event.setCanceled(true);
        }
    }

    private void pickUpTorch(EntityItem entityItem, ItemStack itemStack, EntityPlayer player, EnumHand hand)
    {
        // The EntityItem's lifespan represents the amount of time it has left before the torch goes out
        // The EntityItem's age represents how much of that time has already been spent
        // The difference between the two values is the actual amount of time left before the torch should go out
        itemStack.setItemDamage(itemStack.getItemDamage() + entityItem.getAge());
        player.setHeldItem(hand, itemStack);
        PrimalUtilSpawn.playSound(player.world, entityItem, SoundEvents.ENTITY_ITEM_PICKUP);
        entityItem.setDead();
    }

    // Don't know anything about the furnace - what's currently in it, whether it's currently burning, nothing
    // Setting the burn time to 0 prevents the item from being used as fuel
    // @SubscribeEvent
    // public void onFurnaceFuelBurnTime(FurnaceFuelBurnTimeEvent event)
    // {
    //     if (event.getItemStack().getItem() == ItemRegistry.GLOWING_EMBER)
    //     {
    //         event.setBurnTime(200);
    //     }
    //     else
    //     {
    //         System.out.println("Setting burn time to zero");
    //         event.setBurnTime(0); // Prevents all fuel from being added to fuel slot
    //     }
    // }

    // Attempt to make held torches give light - code below didn't work (doesn't actually check for torch yet)
    // It never lit the block below the player sufficiently, sometimes left behind a trail of darkness, etc.

    // private void addLight(World worldObj, BlockPos pos)
    // {
    //     worldObj.setLightFor(EnumSkyBlock.BLOCK, pos, 15);
    //     worldObj.markBlockRangeForRenderUpdate(pos.getX(), pos.getY(), pos.getX(), 12, 12, 12);
    //     worldObj.markBlockRangeForRenderUpdate(pos, pos);
    //     for (EnumFacing facing : EnumFacing.VALUES)
    //     {
    //         worldObj.setLightFor(EnumSkyBlock.BLOCK, pos.offset(facing), 0);
    //     }
    // worldObj.updateLightByType(EnumSkyBlock.BLOCK, (int) pos.getX(),
    //         (int) pos.getY() + 1, (int) pos.getZ());
    // worldObj.updateLightByType(EnumSkyBlock.BLOCK, (int) pos.getX(),
    //         (int) pos.getY() - 1, (int) pos.getZ());
    // worldObj.updateLightByType(EnumSkyBlock.BLOCK,
    //         (int) pos.getX() + 1, (int) pos.getY(), (int) pos.getZ());
    // worldObj.updateLightByType(EnumSkyBlock.BLOCK,
    //         (int) pos.getX() - 1, (int) pos.getY(), (int) pos.getZ());
    // worldObj.updateLightByType(EnumSkyBlock.BLOCK, (int) pos.getX(),
    //         (int) pos.getY(), (int) pos.getZ() + 1);
    // worldObj.updateLightByType(EnumSkyBlock.BLOCK, (int) pos.getX(),
    //         (int) pos.getY(), (int) pos.getZ() - 1);
    // }
    //
    // @SideOnly(Side.CLIENT)
    // @SubscribeEvent
    // public void onTick(TickEvent.PlayerTickEvent event)
    // {
    //     if (event.player != null && event.phase == TickEvent.Phase.START && event.side == Side.CLIENT)
    //     {
    //         BlockPos pos = event.player.getPosition()/*.add(0, -1, 0)*/;
    //
    //         if (event.player.ticksExisted % 20 == 0)
    //         {
    //             System.out.println("Pos: " + pos);
    //         }
    //
    //         addLight(event.player.world, pos);
    //     }
    // }
}
