package com.daeruin.darkestbeforedawn.inventory;

import com.daeruin.darkestbeforedawn.tileentity.TileEntityCampfire;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

public class ItemStackHandlerCampfire extends ItemStackHandler
{

    private final TileEntityCampfire tileEntityCampfire;

    public ItemStackHandlerCampfire(TileEntityCampfire tileEntityCampfire)
    {
        super(3); // Inventory has 3 slots
        this.tileEntityCampfire = tileEntityCampfire; // Needed to set cookTime and totalCookTime timers in setStackInSlot
    }

    // My addition - enforces slot limits when trying to place things in slot via left click.
    public int getSlotStackLimit(int slot)
    {
        int limit;
        switch (slot)
        {
            case TileEntityCampfire.SLOT_INPUT:
                // case TileEntityCampfire.SLOT_OUTPUT:
                limit = 1;
                break;
            case TileEntityCampfire.SLOT_FUEL:
                limit = 4;
                break;
            default:
                limit = 64;
                break;
        }
        return limit;
    }

    // Returns the smaller of the slot's item limit or the item's max stack size
    @Override
    protected int getStackLimit(int slot, @Nonnull ItemStack stack)
    {
        return Math.min(getSlotStackLimit(slot), stack.getMaxStackSize());
    }

    // Same as superclass except for calling my override of getStackLimit and setting timers for input slot
    // TO DO:
    // I don't need to override this method in order to call my overridden getStackLimit -- I only need to override getStackLimit
    // I also don't need to replicate this entire method just to set timers on my input slot.
    // Instead, I could just set the timers, then call super.insertItem
    @Override
    @Nonnull
    @ParametersAreNonnullByDefault
    public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
    {
        if (stack.isEmpty())
            return ItemStack.EMPTY;

        validateSlotIndex(slot);

        ItemStack existing = this.stacks.get(slot);

        int limit = getStackLimit(slot, stack); // Calls my override so it will respect my slot limits

        if (!existing.isEmpty())
        {
            if (!ItemHandlerHelper.canItemStacksStack(stack, existing))
                return stack;

            limit -= existing.getCount();
        }

        if (limit <= 0)
            return stack;

        boolean reachedLimit = stack.getCount() > limit;

        if (!simulate)
        {
            if (existing.isEmpty())
            {
                this.stacks.set(slot, reachedLimit ? ItemHandlerHelper.copyStackWithSize(stack, limit) : stack);
            }
            else
            {
                existing.grow(reachedLimit ? limit : stack.getCount());
            }

            if (slot == TileEntityCampfire.SLOT_INPUT) // If this is the input slot, start up the cookTime and totalCookTime timers
            {
                tileEntityCampfire.setTotalCookTimeForInput(stack);
                tileEntityCampfire.setCookTimer(0);
            }

            onContentsChanged(slot);
        }

        return reachedLimit ? ItemHandlerHelper.copyStackWithSize(stack, stack.getCount() - limit) : ItemStack.EMPTY;
    }

    // Same as superclass, except sets the cookTime and totalCookTime fields in the tile entity
    @Override
    @ParametersAreNonnullByDefault
    public void setStackInSlot(int slot, ItemStack stack)
    {
        validateSlotIndex(slot);

        if (ItemStack.areItemStacksEqual(this.stacks.get(slot), stack))
            return;

        this.stacks.set(slot, stack);

        if (slot == TileEntityCampfire.SLOT_INPUT) // If this is the input slot, start up the cookTime and totalCookTime timers
        {
            tileEntityCampfire.setTotalCookTimeForInput(stack);
            tileEntityCampfire.setCookTimer(0);
        }

        onContentsChanged(slot);
    }

}
