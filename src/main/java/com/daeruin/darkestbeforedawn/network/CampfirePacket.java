package com.daeruin.darkestbeforedawn.network;

import com.daeruin.darkestbeforedawn.SoundRegistry;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.IThreadListener;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CampfirePacket implements IMessage
{
    private int playerId;
    private int messageId;
    private double posX;
    private double posY;
    private double posZ;

    @SuppressWarnings("unused")
    public CampfirePacket()
    {
    }

    public CampfirePacket(EntityPlayer player, int messageId, BlockPos pos)
    {
        this.playerId = player.getEntityId();
        this.messageId = messageId;
        this.posX = pos.getX() + 0.5D;
        this.posY = pos.getY();
        this.posZ = pos.getZ() + 0.5D;
    }

    @Override
    public void fromBytes(ByteBuf buffer)
    {
        this.playerId = buffer.readInt();
        this.messageId = buffer.readInt();
        this.posX = buffer.readInt();
        this.posY = buffer.readInt();
        this.posZ = buffer.readInt();
    }

    @Override
    public void toBytes(ByteBuf buffer)
    {
        buffer.writeInt(playerId);
        buffer.writeInt(messageId);
        buffer.writeDouble(posX);
        buffer.writeDouble(posY);
        buffer.writeDouble(posZ);
    }

    // This is a message to the client
    public static class CampfireHandler implements IMessageHandler<CampfirePacket, IMessage>
    {
        @SuppressWarnings("Convert2Lambda")
        @Override
        public IMessage onMessage(final CampfirePacket message, final MessageContext ctx)
        {
            IThreadListener mainThread = Minecraft.getMinecraft();
            mainThread.addScheduledTask(new Runnable()
            {
                @Override
                public void run()
                {
                    Entity entity = Minecraft.getMinecraft().world.getEntityByID(message.playerId);
                    if (entity instanceof EntityPlayer)
                    {
                        if (message.messageId == 0)
                        {
                            entity.playSound(SoundRegistry.blowing, 1.0F, 1.0F);
                        }
                        if (message.messageId == 1)
                        {
                            Minecraft.getMinecraft().world.spawnParticle(EnumParticleTypes.FLAME, message.posX, message.posY, message.posZ, 0.0D, 0.0D, 0.0D);
                        }
                    }
                }
            });
            return null;
        }
    }
}
