package com.daeruin.darkestbeforedawn.config;

import com.daeruin.darkestbeforedawn.DarkestBeforeDawn;
import com.daeruin.primallib.PrimalLib;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = DarkestBeforeDawn.MODID)
public class DarkestConfig
{
    @Config.Name("Basic Items and Blocks")
    @Config.Comment("Define what items and blocks should be allowed in the game.")
    public static BasicItems basicItems = new BasicItems();

    @Config.Name("Scarcity of Items and Blocks")
    @Config.Comment("Define how easily items and blocks from Darkest Before Dawn can be obtained.")
    public static ItemScarcity itemScarcity = new ItemScarcity();

    @Config.Name("Torch Behavior")
    @Config.Comment("Define how torches behave (duration, brightness, how to pick up).")
    public static TorchBehavior torchBehavior = new TorchBehavior();

    @Config.Name("Campfire Behavior")
    @Config.Comment("Define how campfires behave (fuel, etc.).")
    public static CampfireBehavior campfireBehavior = new CampfireBehavior();

    @Config.Name("Darkness Behavior")
    @Config.Comment("Define how darkness behaves.")
    public static DarknessBehavior darknessBehavior = new DarknessBehavior();

    public static class BasicItems
    {
        // Torch fuel

        @Config.Name("Allow animal fat")
        @Config.Comment("Whether animal fat should drop from animals. If set to true, it also enables rendered animal fat for making torches.")
        public boolean allowAnimalFat = true;

        @Config.Name("Allow resin")
        @Config.Comment("Whether resin should drop from spruce trees. If set to true, it also enables resinous spruce logs and melted resin for making torches.")
        public boolean allowResin = true;

        @Config.Name("Allow charcoal dust")
        @Config.Comment("Whether charcoal dust can be crafted and used to make pitch torches.")
        public boolean allowCharcoalDust = true;

        // Torch types

        @Config.Name("Recipes to remove")
        @Config.Comment("A list of recipes that should be removed. Vanilla torches and certain unrealistic torches from other mods are removed by default. You can enable them by removing them from this list. You could disable Darkest Before Dawn recipes by adding them to this list.")
        public String[] recipesToRemove = {
                "minecraft:torch",
                "tconstruct:stone_torch",
                "bonetorch:boneTorch",
                "jetorches:torch_golden",
                "jetorches:torch_nether",
                "jetorches:torch_obsidian",
                "jetorches:torch_prismarine",
                "jetorches:torch_stone"
        };

        @Config.Name("Allow unlit torches")
        @Config.Comment("Whether unlit torches are allowed in the game. Set to false to disable all recipes for unlit torches. If set to true, you can still disable individual torch recipes.")
        public boolean allowUnlitTorches = true;

        @Config.Name("Allow pitch torches")
        @Config.Comment("Whether pitch can be crafted and used to make pitch torches. The pitch recipe requires resin and charcoal dust, so you must enable those ingredients as well.")
        public boolean allowPitchTorches = true;

        @Config.Name("Allow birch bark torches")
        @Config.Comment("Whether birch bark can be used to craft torches. Requires an item registered as torchFuel, such as melted resin or rendered animal fat.")
        public boolean allowBirchBarkTorches = true;

        @Config.Name("Allow any bark to make a torch")
        @Config.Comment("Set to true to make it so all bark types can be used to make torches. Set to false to make it so only birch bark can be used.")
        public boolean allowAnyBark = false;

        @Config.Name("Allow cattail torches")
        @Config.Comment("Whether cattails can be used to make pitch torches. Any cattail item registered as plantCattail to the Forge OreDictionary will work. This is known to support cattails from Biomes O' Plenty. Darkest Before Dawn does not add its own cattail items.")
        public boolean allowCattailTorches = true;

        @Config.Name("Allow cane torches")
        @Config.Comment("Whether sugar cane can be used to make frayed cane torches. Requires an item registered as torchFuel, such as melted resin or rendered animal fat.")
        public boolean allowCaneTorches = true;

        @Config.Name("Allow spruce cone torches")
        @Config.Comment("Whether spruce cones can be obtained and used to make spruce cone torches. Requires an item registered as torchFuel, such as melted resin or rendered animal fat.")
        public boolean allowSpruceConeTorches = true;

        // Vanilla torches

        @Config.Name("Allow vanilla torches in world gen")
        @Config.Comment("Whether vanilla Minecraft torches should be created during world generation, such as in villages and abandoned mineshafts.")
        public boolean allowVanillaTorches = false;

        @Config.Name("Replacement for vanilla torches")
        @Config.Comment("Specify what block should replace vanilla torches during world generation. To remove vanilla torches completely, leave nothing between the quotation marks. Only works if vanilla torches are not allowed in world gen.")
        public String replacementForVanillaTorches = "darkestbeforedawn:torch_unlit";
    }

    public static class ItemScarcity
    {
        @Config.Name("Sources of animal fat")
        @Config.Comment("Which entities should drop animal fat, and how much they should drop. Syntax is \"modname:entityname,amount\". Separate each entity with a comma.")
        public final String[] animalFatSources = new String[]
                {
                        "minecraft:rabbit,1",
                        "minecraft:wolf,1",
                        "minecraft:ocelot,1",
                        "minecraft:horse,2",
                        "minecraft:mule,2",
                        "minecraft:donkey,2",
                        "minecraft:llama,2",
                        "minecraft:sheep,2",
                        "minecraft:zombie_pigman,2",
                        "minecraft:pig,3",
                        "minecraft:polar_bear,3",
                        "minecraft:cow,3",
                        "minecraft:squid,3"
                };
        // public final Map<String, Integer> animalFatSources = new HashMap<>();
        //
        //     animalFatSources.put("minecraft:rabbit", 1);
        //     animalFatSources.put("minecraft:wolf", 1);
        //     animalFatSources.put("minecraft:ocelot", 1);
        //     animalFatSources.put("minecraft:horse", 2);
        //     animalFatSources.put("minecraft:mule", 2);
        //     animalFatSources.put("minecraft:donkey", 2);
        //     animalFatSources.put("minecraft:llama", 2);
        //     animalFatSources.put("minecraft:sheep", 2);
        //     animalFatSources.put("minecraft:zombie_pigman", 2);
        //     animalFatSources.put("minecraft:pig", 3);
        //     animalFatSources.put("minecraft:polar_bear", 3);
        //     animalFatSources.put("minecraft:cow", 3);
        //     animalFatSources.put("minecraft:squid", 3);

        // @Config.Name("Sources of resin")
        // @Config.Comment("What kinds of blocks should drop resin, and how much they should drop.")
        // public static final Map<String, Integer> resinSources = new HashMap<>();
        //
        // static
        // {
        //     resinSources.put("minecraft:log", 1);
        //     resinSources.put("minecraft:log2", 1);
        // }

        @Config.Name("Resin drop rate")
        @Config.Comment("How much resin should drop when right-clicking resinous spruce logs.")
        @Config.RangeInt(min = 0, max = 64)
        public int resinDropRate = 4;

        @Config.Name("Charcoal dust amount from ashes")
        @Config.Comment("How much charcoal dust should drop when right-clicking the ashes of a campfire with a shovel.")
        @Config.RangeInt(min = 0, max = 64)
        public int charcoalAmtAshes = 4;

        @Config.Name("Charcoal dust amount from logs")
        @Config.Comment("How much charcoal dust should come from cooking logs in a campfire.")
        @Config.RangeInt(min = 0, max = 64)
        public int charcoalAmtLogs = 16;

        @Config.Name("Charcoal dust amount from charcoal")
        @Config.Comment("How much charcoal dust should come from crushing charcoal.")
        @Config.RangeInt(min = 0, max = 64)
        public int charcoalAmtCharcoal = 4;

        @Config.Name("Duration of melted resin")
        @Config.Comment("How many ticks before melted resin turns back into regular resin. Set to 0 or less to disable. 20 ticks = 1 second, 1200 ticks = 1 minute, 24000 ticks = 1 Minecraft day.")
        public int meltedResinDuration = 1200;

        @Config.Name("Duration of rendered fat")
        @Config.Comment("How many ticks before rendered fat turns back into regular fat. Set to 0 or less to disable. 20 ticks = 1 second, 1200 ticks = 1 minute, 24000 ticks = 1 Minecraft day.")
        public int renderedFatDuration = 1200;

        @Config.Name("Duration of glowing ember")
        @Config.Comment("How many ticks before a glowing ember goes out and disappears. Set to 0 or less to disable. 20 ticks = 1 second, 1200 ticks = 1 minute, 24000 ticks = 1 Minecraft day.")
        public int glowingEmberDuration = 200;

        @Config.Name("Spruce cones require sneak")
        @Config.Comment("Whether the player must be sneaking to get spruce cones. Set to true to give players more control over when they want to obtain these drops.")
        public boolean requireSneaking = false;

        @Config.Name("Spruce cone drop chance")
        @Config.Comment({"The chance that spruce cone(s) will drop from spruce leaves.",
                "Min: 0.0", "Max: 1.0"})
        public float spruceConeDropChance = 1.0F;

        @Config.Name("Spruce cone drop rate")
        @Config.Comment("How many spruce cones should drop when breaking spruce leaves.")
        @Config.RangeInt(min = 0, max = 64)
        public int spruceConeDropRate = 1;
    }

    public static class TorchBehavior
    {
        @Config.Name("Ignition sources for held torches")
        @Config.Comment("When you right click on one of these blocks with a held unlit torch, the held torch should light.")
        public String[] ignitionSourcesForItem = {
                "darkestbeforedawn:campfire",
                "darkestbeforedawn:torch_lit",
                "minecraft:torch",
                "minecraft:lit_furnace",
                "minecraft:fire",
                "minecraft:lava",
                "minecraft:flowing_lava"
        };
        @Config.Name("Ignition sources for placed torches")
        @Config.Comment("When you right click on a placed unlit torch with one of these items, the placed torch should light.")
        public String[] ignitionSourcesForBlock = {
                "darkestbeforedawn:torch_lit",
                "darkestbeforedawn:glowing_ember",
                "minecraft:torch"
        };
        @Config.Name("Torches can only be held in hand")
        @Config.Comment("Whether torches must be held directly in the player's hand. If true, torches can only be picked up into the main or off hand slots, and torches will be dropped if put into any other slots.")
        public boolean torchCanOnlyBeHeldInHand = true;

        @Config.Name("Prefer torches in off hand")
        @Config.Comment("If true, torches will automatically go into the off hand slot when picked up, if it's available.")
        public boolean preferOffHand = true;

        @Config.Name("Torch duration")
        @Config.Comment("How long a torch can last before going out. The value is in ticks (20 ticks per second). To last all night, set to 12000. Set to 0 or less to disable. Default is 3000 or 2.5 real-life minutes.")
        public int torchDuration = 3000;

        @Config.Name("Torch smoldering ratio")
        @Config.Comment("How much of a torch's duration is considered smoldering. Smoldering torches are dimmer and give off more smoke, warning you that the torch is about to burn out. This is a percentage. Default is 0.25 (i.e. 25%).")
        public float smolderingRatio = 0.25F;

        @Config.Name("Sneak right-click to pick up")
        @Config.Comment("If true, player must sneak right-click on a torch block in order to pick it up. Left-clicking on the torch will break it and drop a shaft (or stick depending on config settings in PrimalLib). Set to false for regular vanilla behavior.")
        public boolean sneakToPickUp = true;

        @Config.Name("Torch brightness when smoldering")
        @Config.Comment("How bright a smoldering torch is. Default is 5 (slightly dimmer than a redstone torch).")
        @Config.RangeInt(min = 0, max = 15)
        public int smolderingBrightness = 5;

        @Config.Name("Torch brightness at full strength")
        @Config.Comment("How bright a regular torch is. Default is 10 (similar to a nether portal or glowing redstone ore). This should encourage players to seek out campfires, redstone lamps, etc.")
        @Config.RangeInt(min = 0, max = 15)
        public int fullBrightness = 10;

        @Config.Name("Chance of extinguished torch drop")
        @Config.Comment("Percent chance that torch will drop a wooden shaft or stick when it goes out. Set to 0 to prevent torches from dropping anything when they go out. Min 0, max 1.0. Default 0.85.")
        public float torchDropChance = 0.85F;
    }

    public static class CampfireBehavior
    {
        @Config.Name("Remove food recipes from furnace")
        @Config.Comment("If true, food recipes will be removed from the vanilla furnace. Food can be always cooked in a campfire.")
        public boolean removeFoodFromFurnace = true;

        @Config.Name("Alternate furnace recipe")
        @Config.Comment("If true, the furnace must be crafted from bricks, and bricks are made using a campfire.")
        public boolean alternateFurnaceRecipe = true;

        @Config.Name("Cook time for clay")
        @Config.Comment("How long it takes clay bowls and bricks to harden in a campfire.")
        @Config.RangeInt(min = 0, max = 1000000)
        public int clayCookTime = 2500;

        @Config.Name("Burn time for acacia logs")
        @Config.Comment("How long an acacia log will burn in a campfire.")
        @Config.RangeInt(min = 0, max = 1000000)
        public int acaciaBurnTime = 4600;

        @Config.Name("Burn time for birch logs")
        @Config.Comment("How long a birch log will burn in a campfire.")
        @Config.RangeInt(min = 0, max = 1000000)
        public int birchBurnTime = 3200;

        @Config.Name("Burn time for dark oak logs")
        @Config.Comment("How long a dark oak log will burn in a campfire.")
        @Config.RangeInt(min = 0, max = 1000000)
        public int darkOakBurnTime = 3400;

        @Config.Name("Burn time for jungle logs")
        @Config.Comment("How long a jungle log will burn in a campfire.")
        @Config.RangeInt(min = 0, max = 1000000)
        public int jungleBurnTime = 2400;

        @Config.Name("Burn time for oak logs")
        @Config.Comment("How long an oak log will burn in a campfire.")
        @Config.RangeInt(min = 0, max = 1000000)
        public int oakBurnTime = 4000;

        @Config.Name("Burn time for spruce logs")
        @Config.Comment("How long a spruce log will burn in a campfire.")
        @Config.RangeInt(min = 0, max = 1000000)
        public int spruceBurnTime = 2600;

        @Config.Name("Sources of campfire fuel")
        @Config.Comment("Which items and blocks should provide fuel for a campfire, and how many ticks they should burn. 20 ticks is 1 second of game time. Syntax is modname:fuelname,ticks.")
        public final String[] campfireFuel = new String[]
                {
                        "primallib:plant_fiber,20",
                        "primallib:cordage,20",
                        "darkestbeforedawn:tinder,20",
                        "primallib:bark_strips,20",
                        "primallib:bark_oak,40",
                        "primallib:bark_spruce,40",
                        "primallib:bark_birch,40",
                        "primallib:bark_jungle,40",
                        "primallib:bark_acacia,40",
                        "primallib:bark_dark_oak,40",
                        "primallib:twig,40",
                        "darkestbeforedawn:spruce_cone,60",
                        "primallib:wooden_shaft,150",
                        "primallib:branch,200",
                        "basketcase:item_wicker_small,200",
                        "darkestbeforedawn:fireboard,350",
                        "basketcase:item_wicker_medium,350",
                        "basketcase:container_basket_small,350",
                        "basketcase:item_wicker_large,500",
                        "basketcase:container_basket_medium,500",
                        "darkestbeforedawn:torch_unlit,500",
                        "basketcase:container_basket_large,700"
                };
    }

    public static class DarknessBehavior
    {
        @Config.Name("Darkness feature enabled")
        @Config.Comment("Set to true to make it darker at night and underground. This feature uses a lot of processing power. You will be unable to change your gamma settings with this feature enabled.")
        public boolean enableDarkness = true;

        @Config.Name("Darkness level")
        @Config.Comment({"How dark it actually gets. Minimum 0; maximum 1.0; default 1.0. Set to 0 for light levels in the normal vanilla range. Set to higher numbers for darker darkness."})
        @Config.RangeDouble(min = 0, max = 1.0)
        public float darknessLevel = 1.0F;

        @Config.Name("How often to calculate light")
        @Config.Comment("Number of ticks to wait before recalculating how much light there is. Minimum 1; maximum 20 (1 second); default 1. Lower values provide smoother light changes but may slow down the game. Higher values make the game faster.")
        @Config.RangeInt(min = 1, max = 20)
        public int lightCheckFrequency = 1;
    }

    @Mod.EventBusSubscriber
    private static class ConfigEventHandler
    {
        @SubscribeEvent
        public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event)
        {
            if (event.getModID().equals(DarkestBeforeDawn.MODID))
            {
                ConfigManager.sync(DarkestBeforeDawn.MODID, Config.Type.INSTANCE);
            }
        }
    }
}
