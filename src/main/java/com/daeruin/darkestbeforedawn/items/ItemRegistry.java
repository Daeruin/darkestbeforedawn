package com.daeruin.darkestbeforedawn.items;

import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.primallib.items.ItemBase;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.LinkedHashSet;

public final class ItemRegistry
{
    public static final Item GLOWING_EMBER = new ItemExpirable("glowing_ember", DarkestConfig.itemScarcity.glowingEmberDuration, ItemStack.EMPTY);
    public static final Item TINDER = new ItemTinder("tinder");
    public static final Item CLAY_BOWL = new ItemBase("clay_bowl");
    public static final Item CLAY_BOWL_UNCOOKED = new ItemBase("clay_bowl_uncooked");
    public static final Item RESIN = new ItemBase("resin");
    public static final Item CLAY_BOWL_RESIN = new ItemBase("clay_bowl_resin");
    public static final Item CLAY_BOWL_RESIN_MELTED = new ItemExpirable("clay_bowl_resin_melted", DarkestConfig.itemScarcity.meltedResinDuration, new ItemStack(CLAY_BOWL_RESIN)).setContainerItem(ItemRegistry.CLAY_BOWL);
    public static final Item ANIMAL_FAT_RAW = new ItemBase("animal_fat_raw");
    public static final Item CLAY_BOWL_FAT = new ItemBase("clay_bowl_fat");
    public static final Item CLAY_BOWL_FAT_RENDERED = new ItemExpirable("clay_bowl_fat_rendered", DarkestConfig.itemScarcity.renderedFatDuration, new ItemStack(CLAY_BOWL_FAT)).setContainerItem(ItemRegistry.CLAY_BOWL);
    public static final Item DUST_CHARCOAL = new ItemBase("dust_charcoal");
    private static final Item PITCH = new ItemBase("pitch");
    public static final Item FRAYED_CANE = new ItemBase("frayed_cane");
    public static final Item SPRUCE_CONE = new ItemBase("spruce_cone");

    public static LinkedHashSet<Item> getItems()
    {
        LinkedHashSet<Item> ITEMS = new LinkedHashSet<>();

        ITEMS.add(GLOWING_EMBER);
        ITEMS.add(TINDER);
        ITEMS.add(CLAY_BOWL_UNCOOKED);
        ITEMS.add(CLAY_BOWL);
        ITEMS.add(RESIN);
        ITEMS.add(CLAY_BOWL_RESIN);
        ITEMS.add(CLAY_BOWL_RESIN_MELTED);
        ITEMS.add(ANIMAL_FAT_RAW);
        ITEMS.add(CLAY_BOWL_FAT);
        ITEMS.add(CLAY_BOWL_FAT_RENDERED);
        ITEMS.add(DUST_CHARCOAL);
        ITEMS.add(PITCH);
        ITEMS.add(FRAYED_CANE);
        ITEMS.add(SPRUCE_CONE);

        return ITEMS;
    }

    @Mod.EventBusSubscriber
    public static class RegistrationHandler
    {
        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event)
        {
            PrimalUtilReg.registerItems(event, getItems());
        }
    }
}
