package com.daeruin.darkestbeforedawn.recipes;

import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.daeruin.primallib.recipes.DummyRecipe;
import com.daeruin.primallib.util.PrimalUtilRecipe;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;

public class RecipeHandler
{
    // public static void registerSmeltingRecipes()
    // {
    //     GameRegistry.addSmelting(ItemRegistry.CLAY_BOWL_UNCOOKED, new ItemStack(ItemRegistry.CLAY_BOWL), 0.5F);
    //     GameRegistry.addSmelting(ItemRegistry.CLAY_BOWL_FAT_RENDERED, new ItemStack(ItemRegistry.CLAY_BOWL_FAT_RENDERED), 0.1F);
    //     GameRegistry.addSmelting(ItemRegistry.CLAY_BOWL_RESIN_MELTED, new ItemStack(ItemRegistry.CLAY_BOWL_RESIN_MELTED), 0.1F);
    // }

    public static void removeConfigRecipes()
    {
        for (String registryName : DarkestConfig.basicItems.recipesToRemove)
        {
            Block block = Block.getBlockFromName(registryName);

            if (block != null)
            {
                PrimalUtilRecipe.removeCraftingRecipe(block);
            }
            else
            {
                Item item = Item.getByNameOrId(registryName);

                if (item != null)
                {
                    PrimalUtilRecipe.removeCraftingRecipe(item);
                }
            }
        }

        // Remove vanilla furnace recipe
        if (DarkestConfig.campfireBehavior.alternateFurnaceRecipe)
        {
            IForgeRegistry<IRecipe> recipeRegistry = ForgeRegistries.RECIPES;
            Item itemToMatch = Item.getItemFromBlock(Blocks.FURNACE);

            for (IRecipe recipe : recipeRegistry)
            {
                ItemStack recipeOutput = recipe.getRecipeOutput();

                // If the recipe output matches the item we're checking
                if (!recipeOutput.isEmpty() && recipeOutput.getItem() == itemToMatch && recipe.getRegistryName() != null)
                {
                    String s = recipe.getRegistryName().getNamespace();

                    if (s.equals("minecraft"))
                    {
                        // System.out.println("Removing recipe for " + itemToMatch.getRegistryName() + ". Please ignore 'Potentially Dangerous alternative prefix' warning.");
                        IRecipe replacement = new DummyRecipe().setRegistryName(recipe.getRegistryName());
                        recipeRegistry.register(replacement);
                    }
                }
            }
        }
    }

    public static void removeSmeltingRecipes()
    {
        PrimalUtilRecipe.removeSmeltingRecipe(Items.COOKED_PORKCHOP);
        PrimalUtilRecipe.removeSmeltingRecipe(Items.COOKED_BEEF);
        PrimalUtilRecipe.removeSmeltingRecipe(Items.COOKED_CHICKEN);
        PrimalUtilRecipe.removeSmeltingRecipe(Items.COOKED_RABBIT);
        PrimalUtilRecipe.removeSmeltingRecipe(Items.COOKED_MUTTON);
        PrimalUtilRecipe.removeSmeltingRecipe(Items.BAKED_POTATO);
        PrimalUtilRecipe.removeSmeltingRecipe(Items.CHORUS_FRUIT_POPPED);
        PrimalUtilRecipe.removeSmeltingRecipe(Items.COOKED_FISH);

        // for (ItemFishFood.FishType fishType : ItemFishFood.FishType.values())
        // {
        //     if (fishType.canCook())
        //     {
        //         this.addCampfireRecipe(new ItemStack(Items.FISH, 1, fishType.getMetadata()), new ItemStack(Items.COOKED_FISH, 1, fishType.getMetadata()), 200, 0.35F);
        //     }
        // }
    }
}
