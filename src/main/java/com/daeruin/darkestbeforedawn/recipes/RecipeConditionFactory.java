package com.daeruin.darkestbeforedawn.recipes;

import com.daeruin.darkestbeforedawn.config.DarkestConfig;
import com.google.gson.JsonObject;
import net.minecraft.util.JsonUtils;
import net.minecraftforge.common.crafting.IConditionFactory;
import net.minecraftforge.common.crafting.JsonContext;

import java.util.function.BooleanSupplier;

@SuppressWarnings("unused")
public class RecipeConditionFactory implements IConditionFactory
{
    @Override
    public BooleanSupplier parse(JsonContext context, JsonObject json)
    {
        boolean allowCrafting;

        // Get the recipe name from the JSON that's being passed in
        // Expected JSON format:
        // json: {"type":"basketcase:is_recipe_allowed","recipe_name":"large_basket"}
        String recipeName = JsonUtils.getString(json, "recipe_name");

        // Figure out which recipe is being parsed, and check the config value for that recipe
        switch (recipeName)
        {
            case "clay_bowl_uncooked":
            case "clay_bowl_fat":
            case "clay_bowl_resin":
                allowCrafting = DarkestConfig.basicItems.allowUnlitTorches; // All torches require torch fuel, and all torch fuel requires a bowl
                break;
            case "dust_charcoal":
                allowCrafting = DarkestConfig.basicItems.allowCharcoalDust;
                break;
            case "furnace":
                allowCrafting = DarkestConfig.campfireBehavior.alternateFurnaceRecipe;
                break;
            case "pitch":
                allowCrafting = DarkestConfig.basicItems.allowPitchTorches;
                break;
            case "torch_unlit_cane":
                allowCrafting = DarkestConfig.basicItems.allowUnlitTorches && DarkestConfig.basicItems.allowCaneTorches;
                break;
            case "torch_unlit_cattail":
                allowCrafting = DarkestConfig.basicItems.allowUnlitTorches && DarkestConfig.basicItems.allowCattailTorches;
                break;
            case "torch_unlit_generic":
                allowCrafting = DarkestConfig.basicItems.allowUnlitTorches;
                break;
            case "torch_unlit_pitch":
                allowCrafting = DarkestConfig.basicItems.allowUnlitTorches && DarkestConfig.basicItems.allowPitchTorches;
                break;
            default:
                allowCrafting = true;
                break;
        }

        // This lambda stuff apparently turns a boolean into a BooleanSupplier
        return () -> allowCrafting;
    }
}
